@extends('admin.layout')
@section('content')
    <style>
        .choseImage {
            cursor: pointer;
        }

        .cr-boundary {
            width: 570px;
            height: 400px;
            padding: 0;
        }

        .croppie-container {
            padding: 0;
        }
    </style>
    <div style="padding: 30px">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Chỉnh sửa bài viết</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <form class="form-horizontal" method="POST" action="{{ route('backend.news.update') }}">
                    {{ csrf_field() }}
                    {{--start left--}}
                    <input type="hidden" name="id" value="{{$news->id}}">
                    <div class="col-md-9">
                        <div class="box box-info" style="padding: 30px">
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="control-label">Tiêu đề</label>
                                <input id="title" type="text" class="form-control" name="title"
                                       value="{{ $news->title }}"
                                       autofocus>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                                <label for="slug" class="control-label">Đường dẫn</label>
                                <input id="slug" type="text" class="form-control" name="slug" value="{{ $news->slug }}"
                                       autofocus>
                                @if ($errors->has('slug'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="control-label">Địa chỉ</label>
                                <input id="address" type="text" class="form-control" name="address"
                                       value="{{ $news->address }}"
                                       autofocus>
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                                <label for="user_id" class="control-label">Người đăng</label>
                                <input id="user_id" type="text" class="form-control" name="user_id"
                                       value="{{ $news->user_id }}"
                                       autofocus>
                                @if ($errors->has('user_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Ngày hết hạn:</label>

                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="expiration_date" class="form-control"
                                           data-date-format='dd-mm-yy' id="datepicker"
                                           value="{{ $news->expiration_date}}">
                                </div>
                                <!-- /.input group -->
                            </div>

                            <div class="form-group{{ $errors->has('renter') ? ' has-error' : '' }}">
                                <label for="renter" class="control-label">Người Thuê</label>
                                <input id="renter" type="text" class="form-control" name="renter"
                                       value="{{ $news->renter }}"
                                       autofocus>
                                @if ($errors->has('renter'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('renter') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="control-label">Số điện thoại</label>
                                <input id="phone" type="text" class="form-control" name="phone"
                                       value="{{ '0'.$news->phone }}"
                                       autofocus>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('area') ? ' has-error' : '' }}">
                                <label for="area" class="control-label">Diện tích</label>
                                <input id="area" type="text" class="form-control" name="area" value="{{ $news->area }}"
                                       required autofocus>
                                @if ($errors->has('area'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('area') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}">
                                <label for="cost" class="control-label">Giá</label>
                                <input id="cost" type="text" class="form-control" name="cost" value="{{ $news->cost }}"
                                       required autofocus>
                                @if ($errors->has('cost'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cost') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
                                <label for="desc" class="control-label">Mô tả</label>
                                <textarea id="desc" class="form-control" name="desc"
                                          required autofocus>{{ $news->desc }} </textarea>
                                @if ($errors->has('desc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    {{--end left--}}
                    {{--start right--}}
                    <div class="col-md-3">
                        <div class="box box-info" style="padding: 30px">
                            <div class="form-group">
                                <label for="exampleInputFile">Chọn ảnh thumbnail</label>
                                <input type="hidden" class="form-control" name="image"/>
                                <div style="padding: 5px 30px; cursor: pointer" id="upload-demo-i">
                                    <img src="{{$news->thumbnail}}"
                                         class="img-responsive show-image-crop" width="150"/>
                                    <input type="hidden" class="form-control" id="image-base64" name="image"
                                           value="{{$news->thumbnail }}"/>
                                </div>
                                <p class="help-block"></p>
                            </div>
                            <div class="form-group">
                                <label> Kiểu tin</label>
                                <select class="form-control select2" style="width: 100%;" name="type_of_news">
                                    @foreach($type as $type)
                                        <option value="{{$type->id}}" @if($news->type_of_news == $type->id){{'selected'}} @endif >{{$type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- checkbox -->
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="status"
                                           class="minimal" @if($news->status == 1){{'checked'}} @endif />
                                    Trạng thái
                                </label>
                            </div>

                            <div class="form-group">
                                <div class="group-imageDetails">
                                    <label for="">Chọn nhiều ảnh chi tiết</label>
                                    <input type="hidden" id="choseManyImage"/>
                                    @foreach($images as $key => $image)
                                        <div class="form-group parent-input parent-{{$key}}" data-id="{{$key}}">
                                            <label for="">Ảnh chi tiết {{$key + 1}}</label>
                                            <div class="choseImage" data-id="{{$key}}">
                                                <img width="100"
                                                     src="{{$image->image}}"
                                                     class="showImage-{{$key}}">
                                                <input type="hidden" name="imagesDetail[{{$key}}]"
                                                       class="form-control imagesDetail-{{$key}}"
                                                       value="{{ $image->id.';'.$image->image }}"/>
                                            </div>
                                        </div>
                                    @endforeach


                                </div>
                                <button type="button" class="add-input btn btn-sm btn-primary">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                    Thêm ảnh
                                </button>
                            </div>
                            <br>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block">
                                    <i class="fa fa-fw fa-paper-plane-o"></i> Lưu lại
                                </button>
                            </div>
                        </div>
                    </div>
                    {{--end right--}}
                </form>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div id="imageCrop" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class=" text-center">
                                <div id="upload-demo" style="width:570px; height: 400px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="modal-footer">
                    <div class="row">
                        <div class=" hidden">
                            <input type="file" id="upload"/>
                        </div>
                        <div class="col-md-6">
                            <button class=" pull-left btn btn-success upload-result">Cắt Ảnh</button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="pull-right btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
