@extends('admin.layout')

@section('content')
    @if (session('status'))
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        </div>
    @endif



    <div class="col-xs-12" style="padding: 10px 10px">
        {{ Breadcrumbs::render('Tin tức') }}
        <div class="box">
            <div class="box-header">
                <h1 class="box-title"><b>Danh sách thông tin thuê nhà, phòng, căn hộ</b></h1>
                <hr>
                <div class="clearfix">
                    <div class="col-sm-2">
                        <a href="{{url('/backend/news/create')}}" class="btn btn-primary"> <i class="fa fa-plus"></i>
                            Thêm
                            mới tin</a>
                    </div>
                    <div class="col-sm-10">
                        <div class="input-group " style="width: 50%; float: right;">
                            <input type="text" name="table_search"
                                   class="form-control news-filter"
                                   placeholder="Tìm kiếm">
                        </div>
                    </div>
                </div>

            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive ">

                <table class="table table-hover table-bordered">
                    <tr>
                        <th>STT</th>
                        <th>Tiêu đề</th>
                        <th>Địa chỉ</th>
                        <th>Chuyên mục</th>
                        <th>Người đăng</th>
                        <th>Đối tượng</th>
                        <th>SĐT</th>
                        <th>Diện tích</th>
                        <th>Giá</th>
                        {{--<th>Mô tả</th>--}}
                        <th>Ảnh đại diện</th>
                        <th>Loại tin</th>
                        <th>Gói thời gian</th>
                        <th>Thời gian</th>
                        <th>Ngày hết hạn</th>
                        <th>Trạng thái</th>
                        <th>Hành Động</th>
                    </tr>
                    <tbody class="news-data-filter">
                    @foreach($news as $key=>$value)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$value->title}}</td>
                            <td>{{$value->address}}</td>
                            <td>{{$value->category->title}}</td>
                            <td>{{$value->user}}</td>
                            <td>@if($value->renter == 1) Tất cả @elseif($value->renter == 2) Nam @else Nữ @endif</td>
                            <td>{{'0'.$value->phone}}</td>
                            <td>{{$value->area}}m²</td>
                            <td>@if($value->cost >= 1) {{$value->cost}} Triệu/tháng @else {{($value->cost)*1000 }}
                                Nghìn/tháng @endif</td>
                            <td><img src="{{$value->thumbnail}}" width="80"></td>
                            <td>{{$value->typeOfNews->name}}</td>
                            <td>@if($value->date_bundle_id == 1)Theo ngày @elseif($value->date_bundle_id == 2)Theo
                                tuần @else Theo tháng @endif </td>
                            <td>{{$value->date_number_id}}</td>
                            <td>{{ date("d/m/Y", strtotime($value->expiration_date)) }}</td>
                            <td>@if($value->status == 1) <span class="label label-success">Hiện</span> @else <span
                                        class="label label-danger">Ẩn</span> @endif</td>
                            <td width="100">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button"
                                            data-toggle="dropdown">Hành động
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu" style="min-width: 100px">
                                        <li><a href="{{url('/backend/news/view/'.$value->id)}}"
                                            >Xem</a></li>
                                        <li><a href="{{url('/backend/news/edit/'.$value->id)}}">Sửa</a></li>
                                        <li><a href="{{url('/backend/news/delete/'.$value->id)}}"
                                               onclick="return confirm('Are you sure?')"
                                            >Xóa</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        {!! $news->links() !!}
                    </ul>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

@endsection