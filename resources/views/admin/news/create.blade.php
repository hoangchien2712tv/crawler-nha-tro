@extends('admin.layout')
@section('content')
    <style>
        .choseImage {
            cursor: pointer;
        }

        .cr-boundary {
            width: 570px;
            height: 400px;
            padding: 0;
        }

        .croppie-container {
            padding: 0;
        }
    </style>
    <div style="padding: 40px 30px 0 30px">
        {{ Breadcrumbs::render('Thêm mới') }}
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Thêm mới bài viết</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form class="form-horizontal" method="POST" action="{{ route('backend.news.store') }}">
                    {{ csrf_field() }}
                    {{--start left--}}
                    <div class="col-md-9">
                        <div class="box box-info" style="padding: 30px">
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="control-label">Tiêu đề</label>
                                <input id="title" type="text" class="form-control" name="title"
                                       value="{{ old('title') }}"
                                       autofocus>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                                <label for="slug" class="control-label">Đường dẫn</label>
                                <input id="slug" type="text" class="form-control" name="slug" value="{{ old('slug') }}"
                                       autofocus>
                                @if ($errors->has('slug'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('cate_id') ? ' has-error' : '' }}"
                                         style="margin-right: 0">
                                        <label for="cate_id" class="control-label">Chuyên mục</label>
                                        <select class="form-control select2" style="width: 100%;" name="cate_id"
                                                id="cate_id">
                                            @foreach($cate as $item)
                                                <option value="{{$item->id}}">{{$item->title}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('cate_id'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('cate_id') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('user') ? ' has-error' : '' }}"
                                         style="margin: 0">
                                        <label for="user" class="control-label">Người đăng</label>
                                        <input id="user" type="text" class="form-control" name="user"
                                               value="{{ old('user') }}"
                                               autofocus>
                                        @if ($errors->has('user'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('user') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('renter') ? ' has-error' : '' }}"
                                         style="margin-left: 0">
                                        <label for="renter" class="control-label">Người Thuê</label>
                                        <select class="form-control select2" style="width: 100%;" name="renter"
                                                id="renter">
                                            <option value="1">Tất cả</option>
                                            <option value="2">Nam</option>
                                            <option value="3">Nữ</option>
                                        </select>
                                        @if ($errors->has('renter'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('renter') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}"
                                         style="margin-right: 0">
                                        <label for="phone" class="control-label">Số điện thoại</label>
                                        <input id="phone" type="text" class="form-control" name="phone"
                                               value="{{ old('phone') }}"
                                               autofocus>
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('area') ? ' has-error' : '' }}"
                                         style="margin: 0">
                                        <label for="area" class="control-label">Diện tích</label>
                                        <input id="area" type="text" class="form-control" name="area"
                                               value="{{ old('area') }}"
                                                autofocus>
                                        @if ($errors->has('area'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('area') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}"
                                         style="margin-left: 0">
                                        <label for="cost" class="control-label">Giá</label>
                                        <input id="cost" type="text" class="form-control" name="cost"
                                               value="{{ old('cost') }}"
                                                autofocus>
                                        @if ($errors->has('cost'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('cost') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('province_id') ? ' has-error' : '' }}"
                                         style="margin-right: 0">
                                        <label for="province_id" class="control-label">Tỉnh, Thành phố</label>

                                        <select class="form-control select2" style="width: 100%;" name="province_id"
                                                id="province_id">
                                            <option >--Chọn tỉnh thành--</option>
                                            @foreach($province as $prov)
                                                <option value="{{$prov->province_id}}">{{$prov->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('province_id'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('province_id') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('district_id') ? ' has-error' : '' }}"
                                         style="margin: 0">
                                        <label for="district_id" class="control-label">Quận, Huyện</label>
                                        <select class="form-control select2" style="width: 100%;" name="district_id"
                                                id="district_id">
                                            <option >- Chọn Quận, Huyện-</option>
                                        </select>
                                        @if ($errors->has('district_id'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('district_id') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('street_id') ? ' has-error' : '' }}"
                                         style="margin-left: 0">
                                        <label for="street_id" class="control-label">Đường phố</label>
                                        <select class="form-control select2" style="width: 100%;" name="street_id"
                                                id="street_id">
                                            <option value="">- Chọn Đường phố -</option>
                                        </select>
                                        @if ($errors->has('street_id'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('street_id') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="control-label">Địa chỉ chính xác</label>
                                <input id="address" type="text" class="form-control" name="address"
                                       value="{{ old('address') }}"
                                       autofocus>
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
                                <label for="desc" class="control-label">Mô tả</label>
                                <textarea id="desc" class="form-control" name="desc"
                                          required autofocus>{{ old('desc') }} </textarea>
                                @if ($errors->has('desc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desc') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group" style="margin: -10px">
                                        <label> Kiểu tin</label>
                                        <select class="form-control select2 change-type" style="width: 100%;"
                                                id="type_of_news" name="type_of_news">
                                            @foreach($type as $type)
                                                <option value="{{$type->id}}"
                                                        data-day="{{$type->day_bundle}}"
                                                        data-week="{{$type->week_bundle}}"
                                                        data-month="{{$type->month_bundle}}">
                                                    {{$type->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group" style="margin: -10px">
                                        <label> Gói tin</label>
                                        <select class="form-control select2 change-bundle" style="width: 100%;"
                                                name="date_bundle"
                                                id="date_bundle">
                                            @foreach($bundle as $bund)
                                                <option value="{{$bund->id}}">{{$bund->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group" style="margin: -10px">
                                        <label class="chose-date chose-type "> Số ngày</label>

                                        <input type="text" name="date_number" value="3" class="form-control change-date"
                                               id="date_number">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group" style="margin: -10px">
                                        <label>Ngày hết hạn:</label>

                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="expiration_date" class="form-control"
                                                   data-date-format='dd-mm-yy' id="expiration_date">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="info-money pull-right">
                                <span class="expirate-money"></span> vnđ
                            </div>

                        </div>
                    </div>
                    {{--end left--}}
                    {{--start right--}}
                    <div class="col-md-3">
                        <div class="box box-info" style="padding: 30px">
                            <div class="form-group">
                                <label for="exampleInputFile">Chọn ảnh thumbnail</label>
                                <input type="hidden" class="form-control" name="image"/>
                                <div style="padding: 5px 30px; cursor: pointer" id="upload-demo-i">
                                    <img src="https://cdn7.bigcommerce.com/s-3e0mry5w/stencil/35ac6680-10d5-0136-f818-525400970412/img/no-image.svg"
                                         class="img-responsive show-image-crop" width="150"/>
                                    <input type="hidden" class="form-control" id="image-base64" name="image"/>
                                </div>
                                <p class="help-block"></p>
                            </div>

                            <!-- checkbox -->
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="status" class="minimal" checked>
                                    Trạng thái
                                </label>
                            </div>

                            <div class="form-group">
                                <div class="group-imageDetails">
                                    <label for="">Chọn nhiều ảnh chi tiết</label>
                                    <input type="hidden" id="choseManyImage"/>
                                    <br>
                                    <div style="width: 50%; padding: 5px;" class="pull-left parent-input parent-0"
                                         data-id="0">
                                        <label for="">Ảnh chi tiết 1</label>
                                        <div class="choseImage" data-id="0">
                                            <img src="https://cdn7.bigcommerce.com/s-3e0mry5w/stencil/35ac6680-10d5-0136-f818-525400970412/img/no-image.svg"
                                                 class="showImage-0 img-responsive">
                                            <input type="hidden" name="imagesDetail[0]"
                                                   class="form-control imagesDetail-0"/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <button type="button" class="add-input btn btn-sm btn-primary">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                Thêm ảnh
                            </button>
                            <hr>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block">
                                    <i class="fa fa-fw fa-paper-plane-o"></i> Lưu lại
                                </button>
                            </div>
                        </div>
                    </div>
                    {{--end right--}}
                </form>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div id="imageCrop" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class=" text-center">
                                <div id="upload-demo" style="width:570px; height: 400px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="modal-footer">
                    <div class="row">
                        <div class=" hidden">
                            <input type="file" id="upload"/>
                        </div>
                        <div class="col-md-6">
                            <button class=" pull-left btn btn-success upload-result">Cắt Ảnh</button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="pull-right btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection
