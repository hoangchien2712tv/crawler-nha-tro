@extends('admin.layout')

@section('content')
    <div class="col-xs-12" style="padding: 10px 30px">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Xem thông tin chi tiết</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <p><a href="{{url('backend/news/index')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Quay
                        lại</a></p>
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th style="width: 100px">Tiêu đề</th>
                        <th>{{$data->title}}</th>
                    </tr>
                    <tr>
                        <th style="width: 100px">Đường dẫn</th>
                        <th>{{$data->slug}}</th>
                    </tr>
                    <tr>
                        <th style="width: 15px">Địa chỉ</th>
                        <th>{{$data->address}}</th>
                    </tr>

                    <tr>
                        <th style="width: 100px">Người đăng</th>
                        <th>{{$data->user}}</th>
                    </tr>
                    <tr>
                        <th style="width: 15px">Loại tin</th>
                        <th>{{$data->typeOfNews->name}}</th>
                    </tr>
                    <tr>
                        <th style="width: 15px">Gói tin</th>
                        <td>@if($data->date_bundle_id == 1) Theo ngày @elseif($data->date_bundle_id == 2) Theo tuần @else
                                Theo tháng @endif </td>
                    </tr>

                    <tr>
                        <th style="width: 15px">Số lượng</th>
                        <td>{{$data->date_number_id}}</td>
                    </tr>
                    <tr>
                        <th style="width: 100px">Ngày hết hạn</th>
                        <th>{{$data->expiration_date}}</th>
                    </tr>
                    <tr>
                        <th style="width: 100px">Đối tượng</th>
                        <th>@if($data->renter == 1) Tất cả @elseif($data->renter == 2) Nam @else Nữ @endif</th>
                    </tr>
                    <tr>
                        <th style="width: 15px">Số Điện thoại</th>
                        <th>{{'0'.$data->phone}}</th>
                    </tr>
                    <tr>
                        <th style="width: 100px">Diện tích</th>
                        <th>{{$data->area}}m²</th>
                    </tr>
                    <tr>
                        <th style="width: 100px">Giá</th>
                        <th>@if($data->cost >= 1) {{$data->cost}} Triệu/tháng @else {{($data->cost)*1000 }}
                            Nghìn/tháng @endif</th>
                    </tr>
                    <tr>
                        <th style="width: 100px">Mô tả</th>
                        <th>{{html_entity_decode($data->desc)}}</th>
                    </tr>
                    <tr>
                        <th style="width: 100px">Ảnh đại diện</th>
                        <th><img src="{{$data->thumbnail}}" width="150"/></th>
                    </tr>
                    <tr>
                        <th style="width: 100px">Trạng thái</th>
                        <th>@if($data->status == 1) <span class="label label-success">Hiện</span> @else <span
                                    class="label label-danger">Ẩn</span> @endif</th>
                    </tr>
                    <tr>
                        <th style="width: 100px">Ngày tạo</th>
                        <th>{{ date_format($data->created_at, 'Y-m-d H:i:s')}}</th>
                    </tr>
                    <tr>
                        <th style="width: 100px">Ngày Cập nhật</th>
                        <th>{{ date_format($data->updated_at, 'Y-m-d H:i:s')}}</th>
                    </tr>
                    @if($images)
                        <tr>
                            <th style="width: 100px">Ảnh chi tiết</th>
                            <th>
                                @foreach($images as $image)
                                    <div class="pull-left" style="padding: 5px"><img src="{{$image->image}}"
                                                                                     width="150"/></div>
                                @endforeach
                            </th>
                        </tr>
                    @endif
                    {{--<td>{{$value->desc}}</td>--}}
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->


        </div>
    </div>
@endsection