@extends('admin.layout')

@section('content')
    <div class="col-xs-10" style="padding: 10px 30px">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Danh sách loại tin</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive ">
                <div class="col-sm-2">
                    <p><a href="{{url('/backend/typeofnews/create')}}" class="btn btn-primary"> <i class="fa fa-plus"></i> Thêm
                            mới loại tin</a></p>
                </div>
                <table class="table table-hover">
                    <tr>
                        <th>STT</th>
                        <th>tên</th>
                        <th>Đường dẫn</th>
                        <th>Gói ngày</th>
                        <th>Gói tuần (7 ngày)</th>
                        <th>Gói tháng (30 ngày)</th>
                        <th>Hành Động</th>
                    </tr>
                    @foreach($data as $key=>$value)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$value->name}}</td>
                            <td>{{$value->slug}}</td>
                            <td>{{number_format($value->day_bundle,0,",",".")}} đ/ngày</td>
                            <td>{{number_format($value->week_bundle,0,",",".")}} đ/ngày</td>
                            <td>{{number_format($value->month_bundle,0,",",".")}} đ/ngày</td>
                            <td>@if($value->status == 1) <span class="label label-success">Hiện</span> @else <span
                                        class="label label-danger">Ẩn</span> @endif</td>
                            <td width="150">
                                <a href="{{url('/backend/typeofnews/edit/'.$value->id)}}">Sửa</a>
                                <a href="{{url('/backend/typeofnews/delete/'.$value->id)}}"
                                   onclick="return confirm('Are you sure?')"
                                >Xóa</a>
                            </td>

                        </tr>
                    @endforeach
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

@endsection