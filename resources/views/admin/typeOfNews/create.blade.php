@extends('admin.layout')

@section('content')
    <div class="col-sm-8">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Thêm mới loại tin</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                      action="{{ url('/backend/typeofnews/store') }} ">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-4 ">Tên</label>
                        <div class="col-md-10">
                            <input id="title" type="text" class="form-control posts-title" name="name"
                                   value="{{ old('name') }}"
                                   autofocus>
                            @if ($errors->has('name'))
                                <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                        <label for="slug" class="col-md-4 ">Đường dẫn</label>
                        <div class="col-md-10">
                            <input id="slug" type="text" class="form-control posts-slug" name="slug"
                                   value="{{ old('slug') }}"
                                   autofocus>
                            @if ($errors->has('slug'))
                                <span class="help-block">
                        <strong>{{ $errors->first('slug') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('day_bundle') ? ' has-error' : '' }}">
                        <label for="slug" class="col-md-4 ">Gói ngày</label>
                        <div class="col-md-10">
                            <input id="slug" type="text" class="form-control posts-slug" name="day_bundle"
                                   value="{{ old('day_bundle') }}"
                                   autofocus>
                            @if ($errors->has('day_bundle'))
                                <span class="help-block">
                        <strong>{{ $errors->first('day_bundle') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('week_bundle') ? ' has-error' : '' }}">
                        <label for="slug" class="col-md-4 ">Gói tuần</label>
                        <div class="col-md-10">
                            <input id="slug" type="text" class="form-control posts-slug" name="week_bundle"
                                   value="{{ old('week_bundle') }}"
                                   autofocus>
                            @if ($errors->has('week_bundle'))
                                <span class="help-block">
                        <strong>{{ $errors->first('week_bundle') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('month_bundle') ? ' has-error' : '' }}">
                        <label for="slug" class="col-md-4 ">Gói tháng</label>
                        <div class="col-md-10">
                            <input id="slug" type="text" class="form-control posts-slug" name="month_bundle"
                                   value="{{ old('month_bundle') }}"
                                   autofocus>
                            @if ($errors->has('month_bundle'))
                                <span class="help-block">
                        <strong>{{ $errors->first('month_bundle') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>
                                <input type="checkbox" name="status" class="minimal" checked>
                                Trạng thái
                            </label>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 ">
                            <button type="submit" class="btn btn-primary">
                                Lưu lại
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection