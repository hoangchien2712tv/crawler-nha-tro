@extends('admin.layout')
@section('content')

    <div class="col-xs-10" style="padding: 10px 30px">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Danh sách Quyền và nhóm Quyền gán cho User</h3>
            </div>
            <div class="box-body table-responsive ">
                <label for="">Quyền và nhớm Quyền chưa gán</label>
                <form method="POST" action="{{ route('backend.user.assignpertorole') }}"
                      style="display: inherit; width: 100%">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$models->id}}"/>
                    <div class="col-md-4">
                        <div class="form-group">
                            @if($permissions)
                                <h4 for="">Danh sách Quyền</h4>
                                <select multiple class="form-control" name="per_assign[]" style="height: 280px">
                                    @foreach($permissions as $permission)
                                        <option value="{{$permission->rule_name}}">{{$permission->name}}</option>
                                    @endforeach

                                </select>
                            @endif
                            @if($group)
                                <h4 for="">Danh sách nhóm Quyền</h4>
                                <select name="role_assign[]" class="form-control" multiple style="height: 280px">
                                    @foreach($group as $group)
                                        <option value="{{$group->rule_name}}">{{$group->name}}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-block btn-success btn-sm" style="margin-top: 200px;">Thực hiện</button>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <h4 for="">Danh sách nhóm Quyền, Quyền đã gán</h4>
                            <select multiple class="form-control" name="user_assigned[]" style="height: 600px">
                                @foreach($assigned as $assigned)
                                    <option value="{{$assigned->auth_item_name}}">{{$assigned->hasRole->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection