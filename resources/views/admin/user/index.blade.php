@extends('admin.layout')
@section('content')
    <div class="col-xs-12" style="padding: 10px 30px">
        <div class="box">
            <div class="box-header">
                <h1 class="box-title"><b>Danh sách thông tin User</b></h1>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 300px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-body table-responsive ">
                <p><a href="{{url('/backend/user/create')}}" class="btn btn-primary"> <i class="fa fa-plus"></i> Thêm
                        mới User</a></p>
                <table class="table">
                    <thead>
                    <td>STT</td>
                    <td>Username</td>
                    <td>Email</td>
                    <td>Vai trò</td>
                    <td>Ngày tạo</td>
                    <td>Hành động</td>
                    </thead>
                    @foreach($data as $key => $value)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$value->name}}</td>
                            <td>{{$value->email}}</td>
                            <td>@if($value->level == 1) <span class="btn btn-sm btn-info">Thành viên</span>
                                @else
                                    <span class="btn btn-sm btn-info">Khách</span>
                                @endif

                            </td>
                            <td>{{$value->created_at}}</td>
                            <td>
                                <a href="{{url('backend/user/view/'.$value->id)}}"
                                   class="btn btn-success btn-sm">Gán quyền</a>
                                <a href="{{url('backend/user/edit/'.$value->id)}}"
                                   class="btn btn-warning btn-sm">Sửa</a>
                                <a href="{{url('backend/user/delete/'.$value->id)}}"
                                   onclick="return confirm('Are you sure?')"
                                   class="btn btn-danger btn-sm">Xóa</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        {{--{!! $news->links() !!}--}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection