@extends('admin.layout')
@section('content')
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> Tình Thành trong nước</h3>
            </div>
            <div class="box-body">

                <table class="table">
                    <thead>
                    <td>STT</td>
                    <td>Tên hành chính</td>
                    <td>Hành Động</td>
                    </thead>
                    @foreach($data as $key => $value)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$value->name}}</td>

                            <td>
                                <a href="{{url('/backend/category/edit/'.$value->id)}}"
                                   class="btn btn-warning btn-sm">Chỉnh sửa</a>
                                <a href="{{url('/backend/category/delete/'.$value->id)}}"
                                   onclick="return confirm('Are you sure?')"
                                   class="btn btn-danger btn-sm">Xóa</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection