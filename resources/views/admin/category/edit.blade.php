@extends('admin.layout')

@section('content')
    <div class="col-sm-8">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Chỉnh sửa Chuyên mục</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                      action="{{ url('/backend/category/update') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$cate->id}}">
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-4 ">Tiêu đề</label>
                        <div class="col-md-10">
                            <input id="title" type="text" class="form-control posts-title" name="title"
                                   value="{{ $cate->title }}"
                                   autofocus>
                            @if ($errors->has('title'))
                                <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                        <label for="slug" class="col-md-4 ">Đường dẫn</label>
                        <div class="col-md-10">
                            <input id="slug" type="text" class="form-control posts-slug" name="slug"
                                   value="{{ $cate->slug }}"
                                   autofocus>
                            @if ($errors->has('slug'))
                                <span class="help-block">
                        <strong>{{ $errors->first('slug') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('parent') ? ' has-error' : '' }}">
                        <label for="parent" class="col-md-4 ">Chuyên mục cha</label>
                        <div class="col-md-10">
                            <select class="form-control" name="parent">
                                <option value="0">Root</option>
                                @if($data)
                                    @foreach($data as $key => $value)
                                        <option value="{{ $key }} @if($key == $cate->cate_id){{'selected'}} @endif">{{ $value }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if ($errors->has('parent'))
                                <span class="help-block">
                        <strong>{{ $errors->first('parent') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>
                                <input type="checkbox" name="status" class="minimal" @if($cate->status == 1) checked @endif />
                                Trạng thái
                            </label>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 ">
                            <button type="submit" class="btn btn-primary">
                                Lưu lại
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection