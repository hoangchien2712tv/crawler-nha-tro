@extends('admin.layout')

@section('content')
    <div class="col-sm-6">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Thêm mới Khoảng giá</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                      action="{{ url('/backend/area/update') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-4 ">Tiêu đề</label>
                        <div class="col-md-10">
                            <input id="title" type="text" class="form-control posts-title" name="name"
                                   value="{{ $data->name }}"
                                   autofocus>
                            @if ($errors->has('name'))
                                <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                        <label for="slug" class="col-md-4 ">Đường dẫn</label>
                        <div class="col-md-10">
                            <input id="slug" type="text" class="form-control posts-slug" name="slug"
                                   value="{{ $data->slug }}"
                                   autofocus>
                            @if ($errors->has('slug'))
                                <span class="help-block">
                        <strong>{{ $errors->first('slug') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                        <label for="slug" class="col-md-4 ">Đường dẫn</label>
                        <div class="col-md-10">
                            <input id="value" type="text" class="form-control posts-slug" name="value"
                                   value="{{ $data->value }}"
                                   autofocus>
                            @if ($errors->has('value'))
                                <span class="help-block">
                        <strong>{{ $errors->first('value') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>
                                <input type="checkbox" name="status" class="minimal" checked>
                                Trạng thái
                            </label>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 ">
                            <button type="submit" class="btn btn-primary">
                                Lưu lại
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection