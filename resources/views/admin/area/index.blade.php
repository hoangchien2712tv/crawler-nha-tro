@extends('admin.layout')
@section('content')
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> Chuyên mục</h3>
            </div>
            <div class="box-body">
                <p><a href="{{url('/backend/area/create')}}" class="btn btn-primary"> <i class="fa fa-plus"></i>
                        Thêm
                        mới Diện tích</a></p>
                <table class="table">
                    <thead>
                    <td>STT</td>
                    <td>Tên</td>
                    <td>Đường dẫn</td>
                    <td>Giá trị</td>
                    <td>Trạng thái</td>
                    <td>Hành Động</td>
                    </thead>
                    @foreach($data as $key => $value)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$value->name}}</td>
                            <td>{{$value->slug}}</td>
                            <td>{{$value->value}}</td>
                            <td>@if($value->status == 0) <span class="btn btn-sm btn-info">Ẩn</span>
                                @else
                                    <span class="btn btn-sm btn-info">Hiện</span>
                                @endif

                            </td>
                            <td>
                                <a href="{{url('/backend/area/edit/'.$value->id)}}"
                                   class="btn btn-warning btn-sm">Chỉnh sửa</a>
                                <a href="{{url('/backend/area/delete/'.$value->id)}}"
                                   onclick="return confirm('Are you sure?')"
                                   class="btn btn-danger btn-sm">Xóa</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection