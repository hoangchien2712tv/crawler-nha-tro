@extends('admin.layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card-box">
                    <form method="POST" class="form" action="{{ route('backend.role.create') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label" for="name">Tên nhóm quyền, hoặc quyền</label>
                            <input class="form-control" id="name" name="name" placeholder="Nhập tên nhóm quyền..." type="text" value="{{ old('name') }}">
                            @if($errors->has('name'))
                                <div class="help-block">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="rule_name">Tên quy tắc</label>
                            <input id="slug" name="rule_name" class="form-control" placeholder="Nhập tên quy tắc..." type="text" value="{{ old('rule_name') }}">
                            @if($errors->has('rule_name'))
                                <div class="help-block">{{ $errors->first('rule_name') }}</div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label class="control-label">Mô tả quyền hạn</label>
                            <textarea class="form-control" name="desc" rows="5">{{ old('desc') }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="sel1">Lựa chọn:</label>
                            <select class="form-control" id="sel1" name="type" >
                                <option value="2">Quyền</option>
                                <option value="1">Nhóm Quyền</option>
                            </select>
                        </div>
                            <div class="form-group">

                                <div class="form-group m-b-0">
                                    <button type="submit" class="btn btn-success waves-effect waves-light">Thêm mới</button>
                                    <button type="reset" class="btn btn-danger waves-effect waves-light">Reset</button>
                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection