@extends('admin.layout')
@section('content')

    <div class="col-sm-12" style="padding: 10px 30px">
        <div class="box">
            <div class="box-header" style="margin: 10px;">
                <h3 class="box-title">Gán quyền cụ thể cho nhóm</h3>
            </div>
            <div class="box-body">
                <form method="POST" action="{{ url('/backend/role/assignpertorole') }}"
                      style="display: inherit; width: 100%">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{$models->rule_name}}" name="rule_name">
                    <input type="hidden" value="{{$models->id}}" name="id">
                    <div class="col-md-4">
                        <div class="form-group">
                            <select multiple class="form-control" name="role_assign[]" style="height: 550px">
                                @if($permissions)
                                    @foreach($permissions as $permission)
                                        <option value="{{$permission->rule_name}}">{{$permission->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-block btn-success btn-sm" style="margin-top: 200px;">Thực hiện</button>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <select multiple class="form-control" name="role_assigned[]" style="height: 550px">
                                @foreach($assigned as $assign)
                                    <option value="{{$assign->child}}">{{$assign->role->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection