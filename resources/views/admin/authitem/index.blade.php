@extends('admin.layout')

@section('content')
    @if (session('status'))
        <div class="col-sm-12">
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        </div>
    @endif
    <div class="col-xs-12" style="padding: 10px 30px">
        <p><a href="{{url('/backend/role/create')}}" class="btn btn-primary"> <i class="fa fa-plus"></i>
                Thêm mới</a></p>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Danh sách Quyền</h3>
            </div>
            <div class="box-body table-responsive ">
                <table class="table">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên Quyền</th>
                        <th>Vai trò</th>
                        <th>Tên quy tắc</th>
                        <th>Mô tả</th>
                        <th>Hành Động</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($permissions)
                        @foreach($permissions as $key => $permission)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$permission->name}}</td>
                                <td>@if($permission->type == 2) Permission @endif</td>
                                <td>{{$permission->rule_name}}</td>
                                <td>{{$permission->desc}}</td>
                                <td>
                                    <a href="{{url('/backend/role/edit/'.$permission->id)}}"
                                       class="btn btn-warning btn-sm">chỉnh sửa</a>
                                    <a href="{{url('/backend/role/delete/'.$permission->id)}}"
                                       onclick="return confirm('Are you sure?')"
                                       class="btn btn-sm btn-danger">xóa</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
        </div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Danh sách nhóm Quyền</h3>
            </div>
            <div class="box-body table-responsive ">
                <table class="table">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên Nhóm</th>
                        <th>Vai trò</th>
                        <th>Tên Quy tắc</th>
                        <th>Mô tả</th>
                        <th>Hành động</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($groupPer)
                        @foreach($groupPer as $k => $value)
                            <tr>
                                <td>{{$k+1}}</td>
                                <td>{{$value->name}}</td>
                                <td>@if($value->type == 1) GoupPermissionr @endif</td>
                                <td>{{$value->rule_name}}</td>
                                <td>{{$value->desc}}</td>
                                <td>
                                    <a href="{{url('/backend/role/view/'.$value->id)}}"
                                       class="btn btn-primary btn-sm">gán quyền</a>
                                    <a href="{{url('/backend/role/edit/'.$value->id)}}"
                                       class="btn btn-warning btn-sm">chỉnh sửa</a>
                                    <a href="{{url('/backend/role/delete/'.$value->id)}}"
                                       onclick="return confirm('Are you sure?')"
                                       class="btn btn-sm btn-danger">xóa</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection