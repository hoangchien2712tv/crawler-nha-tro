<style>
    .eye-toggle {
        height: inherit;
        cursor: pointer;
    }

    .eye-toggle-hide {
        display: none;
    }

    .eye-toggle i {
        padding: 9.5px;
    }

    #current, #password, #password-confirm {
        height: 30px;
        width: 88%;
        float: left;
        border: none;
        outline: none;
    }
</style>
@extends('admin.layout')

@section('content')


    @if (session('success'))
        <div class="container">
            <div class="col-md-12">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif
    @if (session('failure'))
        <div class="container">
            <div class="col-md-12">
                <div class="alert alert-danger">
                    {{ session('failure') }}
                </div>
            </div>
        </div>
    @endif

    <div class="col-xs-6" style="padding: 10px 30px">
        <div class="box">
            <br>
            <div class="box-header">
                <h1 class="box-title"><b>Thay đổi thông tin User</b></h1>
            </div>

            <div class="box-body ">
                <form class="form-horizontal" method="POST"
                      action="{{url('/update-profile')}}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Tên User</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}"
                                   autofocus/>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-md-4 control-label">Địa chỉ E-Mail</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{$user->email}}"/>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif

                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Lưu lại
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xs-6" style="padding: 10px 30px">
        <div class="box">
            <br>
            <div class="box-header">
                <h1 class="box-title"><b>Thay đổi password</b></h1>
            </div>

            <div class="box-body ">
                <form class="form-horizontal" method="POST"
                      action="{{url('/change-password')}}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="current" class="col-md-4 control-label">mật khẩu hiện tại</label>

                        <div class="col-md-6">
                            <div class="form-control" style="padding: 1px">
                                <input id="current" type="password"
                                       name="current">
                                <div class="eye-toggle eye-toggle-show"><i class="fa fa-fw fa-eye"></i></div>
                                <div class="eye-toggle eye-toggle-hide"><i class="fa fa-fw fa-eye-slash"></i>
                                </div>
                            </div>
                            @if ($errors->has('current'))
                                <span class="help-block">
                            <strong>{{ $errors->first('current') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-md-4 control-label">mật khẩu mới</label>

                        <div class="col-md-6">
                            <div class="form-control" style="padding: 1px">
                                <input id="password" type="password" name="password">
                                <div class="eye-toggle eye-toggle-show"><i class="fa fa-fw fa-eye"></i></div>
                                <div class="eye-toggle eye-toggle-hide"><i class="fa fa-fw fa-eye-slash"></i></div>
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="col-md-4 control-label">nhập lại mật khẩu mới</label>

                        <div class="col-md-6">
                            <div class="form-control" style="padding: 1px">
                                <input id="password-confirm" type="password"
                                       name="password_confirmation">
                                <div class="eye-toggle eye-toggle-show"><i class="fa fa-fw fa-eye"></i></div>
                                <div class="eye-toggle eye-toggle-hide"><i class="fa fa-fw fa-eye-slash"></i>
                                </div>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Lưu password
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
