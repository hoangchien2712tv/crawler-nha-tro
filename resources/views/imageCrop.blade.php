<html lang="en">
<head>
    <title>Laravel - jquery ajax crop image before upload using croppie plugins</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script>
    <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/croppie.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Laravel crop image before upload using croppie plugins</div>
        <div class="panel-body">


            <div class="row">
                {{--<div class="col-md-4 text-center">--}}
                {{--<div id="upload-demo" style="width:350px"></div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4" style="padding-top:30px;">--}}
                {{--<strong>Select Image:</strong>--}}
                {{--<br/>--}}
                {{--<input type="file" id="upload" class="upload-result">--}}
                {{--<br/>--}}
                {{--<button class="btn btn-success upload-result">Upload Image</button>--}}
                {{--</div>--}}
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open
                    Modal
                </button>


                <div class="col-md-4" style="">
                    <div id="upload-demo-i"
                         style="background:#e1e1e1;width:300px;padding:30px;height:300px;margin-top:30px"></div>
                </div>
            </div>


        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class=" text-center">
                            <div id="upload-demo" style="width:350px"></div>
                        </div>
                        <div>
                            <strong>Select Image:</strong>
                            <br/>
                            <input type="file" id="upload" class="upload-result">
                            <br/>
                            <button class="btn btn-success upload-result">Upload Image</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 200,
            height: 200,
            // type: 'circle'
        },
        boundary: {
            width: 250,
            height: 250
        }
    });


    $('#upload').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function () {
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
    });


    $('.upload-result').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            $.ajax({
                url: "/image-crop",
                type: "POST",
                data: {"image": resp},
                success: function (data) {
                    html = '<img src="' + resp + '" />';
                    $("#upload-demo-i").html(html);
                }
            });
        });
    });


</script>


</body>
</html>