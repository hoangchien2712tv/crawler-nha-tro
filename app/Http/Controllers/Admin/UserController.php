<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use phpDocumentor\Reflection\Types\Integer;
use App\Http\Requests\Admin\UserRequest;
use App\Models\AuthItem;
use App\Models\AuthItemChild;
use App\Models\AuthAssign;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
//        $data = (new User())->hasAllPerMission('user-index');
//        dd($data);
        $data = User::all();
        return view('admin.user.index', ['data' => $data]);
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        if ($user->save()) {
            return redirect()->to('backend/user/index')->with('info', 'Thêm mới tài khoản thành công');
        }
    }

    public function edit($id)
    {
        $user = $this->model($id);
        return view('admin.user.update', ['model' => $user]);
    }

    public function update( Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = $this->model($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        if ($user->save()) {
            return redirect()->to('backend/user')->with('info', 'Cập nhật tài khoản thành công');
        }

    }


    public function view($id)
    {
        $models = $this->model($id);

        $assigned = AuthAssign::where('user_id', $id)->get();
        $notIn = [];
        foreach ($assigned as $assign) {
            $notIn[] = $assign->auth_item_name;
        }

        $permissions = AuthItem::where('type', 2)->whereNotIn('rule_name', $notIn)->get();
        $group = AuthItem::where('type', 1)->whereNotIn('rule_name', $notIn)->get();

        return view('admin.user.viewrole',
            [
                'models' => $models,
                'permissions' => $permissions,
                'group' => $group,
                'assigned' => $assigned,
            ]);
    }

    public function assignPerToRole(Request $request)
    {

        if ($request->role_assign) {
            foreach ($request->role_assign as $roleValue) {
                $models = new AuthAssign();
                $models->user_id = $request->id;
                $models->auth_item_name = $roleValue;
                $models->save();
            }

        }

        if ($request->per_assign) {
            foreach ($request->per_assign as $perValue) {
                $models = new AuthAssign();
                $models->user_id = $request->id;
                $models->auth_item_name = $perValue;
                $models->save();
            }

        }

        if ($request->user_assigned) {

            foreach ($request->user_assigned as $value) {
                $models = new AuthAssign();
                $models::where('auth_item_name', $value)->delete();
            }
        }
        return redirect()->to('backend/user/view/' . $request->id);
    }

    public function profile()
    {
        return view('auth.profile', ['user' => Auth::user()]);
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'current' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        $user = User::find(Auth::id());
        $hashedPassword = $user->password;

        if (Hash::check($request->current, $hashedPassword)) {
            //Change the password
            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();

            $request->session()->flash('success', 'Your password has been changed.');

            return back();
        }

        $request->session()->flash('failure', 'Your password has not been changed.');
        return back();
    }

    public function updateProfile(Request $request){
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
        ]);
        $user = User::find(Auth::id());
        $user->name = $request->name;
        $user->email = $request->email;
       if( $user->save()) {
        $request->session()->flash('success', 'Your password has been changed.');
       } else {
           $request->session()->flash('failure', 'Your password has not been changed.');
       }
        return back();
    }

    public function delete($id)
    {
        $data = $this->model($id);
        $data->delete();
        return redirect()->to('backend/user/index');
    }

    public function model($id)
    {
        $model = \App\User::find($id);
        if (!$model) {
            return 'error ' . $id;
        }
        return $model;

    }


}
