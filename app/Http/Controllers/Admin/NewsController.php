<?php

namespace App\Http\Controllers\Admin;

use App\Models\District;
use App\Models\Province;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Images;
use App\Models\TypeOfNews;
use App\Models\Category;
use App\Models\TimeBundle;
use App\Models\Street;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::orderBy('id', 'desc')->paginate(10);
        return view('admin.news.index',
            [
                'news' => $news
            ]);
    }

    /*live search*/
    function liveSearch(Request $request)
    {
        if ($request->ajax()) {
            $output = '';
            $query = $request->get('query');
            if ($query != '') {
                $data = News::where('title', 'like', '%' . $query . '%')
                    ->orWhere('address', 'like', '%' . $query . '%')
                    ->orWhere('user', 'like', '%' . $query . '%')
                    ->orWhere('renter', 'like', '%' . $query . '%')
                    ->orWhere('phone', 'like', '%' . $query . '%')
                    ->orWhere('area', 'like', '%' . $query . '%')
                    ->orWhere('cost', 'like', '%' . $query . '%')
                    ->orWhere('phone', 'like', '%' . $query . '%')
                    ->orWhere('type_of_news', 'like', '%' . $query . '%')
                    ->orderBy('id', 'desc')
                    ->get();
            } else {
                $data = News::orderBy('id', 'desc')->get();
            }

            $total_row = $data->count();
            if ($total_row > 0) {
                $output = $this->render_html($data);
            } else {
                $output = '<tr><td colspan="13">Không tìm thấy kết quả nào</td></tr>';
            }
            $data = array(
                'table_data' => $output
            );
            echo json_encode($data);
        }

    }

    public function render_html($news)
    {
        $output = '';

        foreach ($news as $key => $value) {
            $output .= '<tr>';
            $output .= '<td>' . ($key + 1) . '</td>';
            $output .= '<td>' . $value->title . '</td>';
            $output .= '<td>' . $value->address . '</td >';
            $output .= '<td>' . $value->category->title . '</td >';
            $output .= '<td>' . $value->user . '</td >';
            $output .= '<td>';
            if ($value->renter == 1) {
                $output .= 'Tất cả ';
            } elseif ($value->renter == 2) {
                $output .= 'Nam';
            } else {
                $output .= 'Nữ';
            };
            $output .= '</td >';
            $output .= '<td>' . '0' . $value->phone . '</td >';
            $output .= '<td>' . $value->area . 'm²' . '</td >';
            $output .= '<td>';
            if ($value->cost >= 1) {
                $output .= $value->cost . 'Triệu/tháng';
            } else {
                $output .= ($value->cost) * 1000 . 'Nghìn/tháng';
            }
            $output .= '</td >';
            $output .= '<td><img src="' . $value->thumbnail . '" width="80"/></td >';
            $output .= '<td>' . $value->typeOfNews->name . '</td>';
            $output .= '<td>';
            if ($value->date_bundle_id == 1) {
                $output .= 'Theo ngày';
            } elseif ($value->date_bundle_id == 2) {
                $output .= 'Theo tuần';
            } else {
                $output .= 'Theo tháng';
            }
            $output .= '</td>';
            $output .= '<td>' . $value->date_number_id . '</td>';
            $output .= '<td>' . date("d/m/Y", strtotime($value->expiration_date)) . '</td>';
            $output .= '<td>';
            if ($value->status == 1) {
                $output .= '<span class="label label-success">Hiện</span>';
            } else {
                $output .= '<span class="label label-danger">Ẩn</span>';
            }
            $output .= '</td>';

            $output .= ' <td width = "100" >';
            $output .= '<div class="dropdown" > ';
            $output .= '<button class="btn btn-primary dropdown-toggle" type = "button"
                                            data-toggle = "dropdown"> Hành động <span class="caret"></span ></button >';
            $output .= '<ul class="dropdown-menu" style = "min-width: 150px" > ';
            $output .= '<li ><a href = "' . url('/backend/news/view/' . $value->id) . '" > Xem</a ></li > ';
            $output .= '<li ><a href = "' . url('/backend/news/edit/' . $value->id) . '" > Sửa</a ></li > ';
            $output .= '<li ><a href = "' . url('/backend/news/delete/' . $value->id) . '" class="confirm" > Xóa</a ></li > ';
            $output .= '</ul > ';
            $output .= '</div > ';
            $output .= '</td > ';
            $output .= '</tr > ';
        }
        return $output;
    }

    /*live search*/

    public function view($id)
    {
        $data = News::find($id);
        $images = Images::where('news_id', $id)->get();
        return view('admin.news.view',
            [
                'data' => $data,
                'images' => $images,
            ]);
    }


    public function create()
    {
        $type = TypeOfNews::all();
        $cate = Category::all();
        $bundle = TimeBundle::all();
        $province = Province::all();
        $district = District::all();

        return view('admin.news.create', [
            'type' => $type,
            'cate' => $cate,
            'bundle' => $bundle,
            'province' => $province,
            'district' => $district,
        ]);
    }

    public function getDistrict(Request $request)
    {
        if ($request->ajax()) {
            $parent = $request->get('province_id');
            $query = District::where('province_id', $parent)->get();
            $output = '';
            foreach ($query as $qr) {
                $output .= ' < option value = "' . $qr->district_id . '" > ' . $qr->name . '</option > ';
            }
            echo json_encode($output);
        }
    }

    public function getStreet(Request $request)
    {
        if ($request->ajax()) {
            $parent = $request->get('district_id');
            $query = Street::where('district_id', $parent)->get();
            $output = '';
            foreach ($query as $qr) {
                $output .= ' < option value = "' . $qr->id . '" > ' . $qr->name . '</option > ';
            }
            echo json_encode($output);
        }
    }


    public function store(Request $request)
    {
        $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";
        $phone = "/^[0][1-9]\d{8}$|^[0][1-9]\d{9}$/";
        $this->validate($request, [
            'title' => 'required | min:5 | unique:news',
            'slug' => 'required | min:5 | unique:news',
            'address' => 'required',
            'user' => 'required',
            'cate_id' => 'required',
            'renter' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'phone' => array('required', 'regex:' . $phone),
            'renter' => 'required',
            'cost' => array('required', 'regex:' . $regex),
            'area' => array('required', 'regex:' . $regex),
            'desc' => 'required',
            'image' => 'required',

        ]);

        if (!isset($request->_token)) {
            return redirect()->to('/backend/news/create')->with('status', 'Thông tin thiếu');
        }

        $news = New News();
        $news->title = $request->title;
        $news->slug = $request->slug;
        $news->cate_id = $request->cate_id;
        $news->address = $request->address;
        $news->user = $request->user;
        $news->renter = $request->renter;
        $news->phone = (int)$request->phone;
        $news->province_id = $request->province_id;
        $news->district_id = $request->district_id;
        $news->area = $request->area;
        $news->area_id = $this->areaRound($request->area);
        $news->cost = $request->cost;
        $news->cost_id = $this->costRound($request->cost);
        $news->desc = $request->desc;
        $news->type_of_news = $request->type_of_news;
        $news->date_bundle_id = $request->date_bundle;
        $news->date_number_id = $request->date_number;
        $date = str_replace('/', ' - ', $request->expiration_date);
        $date = date('Y-m-d', strtotime($date));
        $news->expiration_date = $date;
        $news->status = $request->status == 'on' ? 1 : 0;
        $news->thumbnail = $this->convertBase64ToImage($request->image);

        if ($news->save()) {
            if (!empty($request->imagesDetail)) {
                foreach ($request->imagesDetail as $key => $image) {
                    $Img = new Images();
                    $Img->news_id = $news->id;
                    $Img->image = str_replace(url('/'), '', $image);
                    $Img->save();
                }
            }
            return redirect()->to('/backend/news/index')->with('status', 'Thêm mới tin thành công');
        }

    }

    public function edit($id)
    {
        $news = News::find($id);

        $images = Images::where('news_id', $id)->get();
        $type = TypeOfNews::all();
        $cate = Category::all();
        $bundle = TimeBundle::all();
        $province = Province::all();
        $district = District::where('province_id', $news->province_id)->get();
        return view('admin.news.edit',
            [
                'news' => $news,
                'images' => $images,
                'type' => $type,
                'cate' => $cate,
                'bundle' => $bundle,
                'province' => $province,
                'district' => $district,
            ]);
    }


    public function update(Request $request)
    {
//        dd($_POST);
        $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";
        $phone = "/^[0][1-9]\d{8}$|^[0][1-9]\d{9}$/";
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'address' => 'required',
            'user' => 'required',
            'cate_id' => 'required',
            'renter' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'phone' => array('required', 'regex:' . $phone),
            'renter' => 'required',
            'cost' => array('required', 'regex:' . $regex),
            'area' => array('required', 'regex:' . $regex),
            'desc' => 'required',
            'image' => 'required',
        ]);

        $news = News::find($request->id);
        $news->title = $request->title;
        $news->slug = $request->slug;
        $news->cate_id = $request->cate_id;
        $news->address = $request->address;
        $news->user = $request->user;
        $news->renter = $request->renter;
        $news->phone = (int)$request->phone;
        $news->province_id = $request->province_id;
        $news->district_id = $request->district_id;
        $news->area = $request->area;
        $news->area_id = $this->areaRound($request->area);
        $news->cost = $request->cost;
        $news->cost_id = $this->costRound($request->cost);
        $news->desc = $request->desc;
        $news->type_of_news = $request->type_of_news;
        $news->date_bundle_id = $request->date_bundle;
        $news->date_number_id = $request->date_number;
        $date = str_replace('/', ' - ', $request->expiration_date);
        $date = date('Y-m-d', strtotime($date));
        $news->expiration_date = $date;
        $news->status = $request->status == 'on' ? 1 : 0;
        $news->thumbnail = $this->convertBase64ToImage($request->image);

        if ($this->is_base64($request->image) == true) {
            $news->thumbnail = $this->convertBase64ToImage($request->image);
        } else {
            $news->thumbnail = $request->image;
        }
        if ($news->save()) {
            if (!empty($request->imagesDetail)) {
                $oldImage = Images::select('id')->where('news_id', $request->id)->get()->toArray();
                $olddata = [];
                foreach ($oldImage as $old) {
                    $olddata[] = $old['id'];
                }

                $idalive = [];
                foreach ($request->imagesDetail as $key => $image) {
                    if (isset(explode(';', $image)[1])) {
                        $idalive[$key] = (int)explode(';', $image)[0];
                        // $Img = Images::find(explode(';', $image)[0]);
                        $Img = Images::find($idalive[$key]);
                        $Img->image = explode(';', $image)[1];
                    } else {
                        $Img = new Images();
                        $Img->news_id = $news->id;
                        $Img->image = str_replace(url('/'), '', $image);
                    }
                    $Img->save();
                }

                $deleteImg = array_diff($olddata, $idalive);
                Images::whereIn('id', $deleteImg)->delete();
            }
            return redirect()->to('/backend/news/index')->with('status', 'Cập nhật tin thành công');
        }


    }


    public function delete($id)
    {
        $images = Images::where('news_id', $id)->get();
        foreach ($images as $image) {
            Images::find($image->id)->delete();
        }
        News::find($id)->delete();
        return redirect()->to('/backend/news/index')->with('status', 'Xóa tin thành công');

    }


    function is_base64($s)
    {
        // Check if there are valid base64 characters
        if (!preg_match(' /^[a - zA - Z0 - 9\/\r\n +]*={
                0,2}$/', $s)) return false;

        // Decode the string in strict mode and check the results
        $decoded = base64_decode($s, true);
        if (false === $decoded) return false;

        // Encode the string again
        if (base64_encode($decoded) != $s) return false;

        return true;
    }

    public function convertBase64ToImage($photo = null)
    {


        if (!empty($photo)) {
            $photo = str_replace('data:image/png;base64,', '', $photo);
            $photo = str_replace(' ', ' + ', $photo);
            $photo = str_replace('data:image/jpeg;base64,', '', $photo);
            $photo = str_replace('data:image/gif;base64,', '', $photo);

            $entry = base64_decode($photo);
            $image = @imagecreatefromstring($entry);

            $fileName = date('Ymd') . time() . ".jpg";
            $directory = $this->pathUrl($fileName);

//            header('Content - Type: image / jpeg');
//            if (!empty($path)) {
//                if (file_exists($path)) {
//                    unlink($path);
//                }
//            }

            $saveImage = @imagejpeg($image, public_path($directory));

            @imagedestroy($image);
            if ($saveImage) {
                return $directory;
            } else {
                return false; // image not saved
            }
        }
    }

    public function pathUrl($fileName)
    {
        $publicPath = public_path('/uploads/images/');
        $this->mkdir($publicPath);
//      $pathDir = $publicPath . date("Y/m/d");
        return '/uploads/images/' . date("Y/m/d") . '/' . $fileName;
    }

    public function mkdir($path)
    {
        if (!is_dir($path . date("Y/m/d"))) {
            mkdir($path . date("Y/m/d"), 0755, true);
        }
    }

    public function areaRound($area)
    {
        if ($area < 20) {
            return 1;
        } elseif ($area < 30 && $area >= 20) {
            return 2;
        } elseif ($area < 50 && $area >= 30) {
            return 3;
        } elseif ($area < 60 && $area >= 50) {
            return 4;
        } elseif ($area < 70 && $area >= 60) {
            return 5;
        } elseif ($area < 80 && $area >= 70) {
            return 6;
        } elseif ($area < 90 && $area >= 80) {
            return 7;
        } elseif ($area < 100 && $area >= 90) {
            return 8;
        } elseif ($area >= 100) {
            return 9;
        }
    }


    public function costRound($cost)
    {
        if ($cost < 1) {
            return 1;
        } elseif ($cost < 2 && $cost >= 1) {
            return 2;
        } elseif ($cost < 3 && $cost >= 2) {
            return 3;
        } elseif ($cost < 5 && $cost >= 3) {
            return 4;
        } elseif ($cost < 7 && $cost >= 5) {
            return 5;
        } elseif ($cost < 10 && $cost >= 7) {
            return 6;
        } elseif ($cost < 15 && $cost >= 10) {
            return 7;
        } elseif ($cost >= 15) {
            return 8;
        }
    }
}
