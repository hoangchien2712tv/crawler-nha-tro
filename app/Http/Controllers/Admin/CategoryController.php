<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use DB;

class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::all();
        return view('admin.category.index', ['data' => $data]);
    }

    public function create()
    {
        $data = (new Category())->getCategoryParent();
        return view('admin.category.create',
            ['data' => $data]);
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'title' => 'required|string|max:255|unique:category',
            'slug' => 'required|string|max:255|unique:category',
            'parent' => 'required',
            'status' => 'required'
        ]);
        $models = new Category();
        $models->title = $request->title;
        $models->slug = $request->slug;
        $models->parent = $request->parent;
        $models->status = $request->status == 'on' ? 1 : 0;
        if ($models->save()) {
            return redirect()->to('/backend/category/index')->with('status', 'Thêm mới chuyên mục thành công');
        }
    }

    public function edit($id)
    {
        $data = (new Category())->getCategoryParent();
        $cate = $this->model($id);

        return view('admin.category.edit',
            [
                'data' => $data,
                'cate' => $cate,
            ]);
    }

    public function update(Request $request)
    {
        $this->validate(request(), [
            'title' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
            'parent' => 'required',
            'status' => 'required',
            'id' => 'required',
        ]);
        $models = $this->model($request->cate_id);

        $models->title = $request->title;
        $models->slug = $request->slug;
        $models->parent = $request->parent;
        $models->status = $request->status == 'on' ? 1 : 0;
        if ($models->save()) {
            return redirect()->to('/backend/category/index')->with('status', 'Cập nhật chuyên mục thành công');
        }
    }

    public function model($id)
    {
        $model = Category::find($id);
        if (!$model) {
            return 'error ' . $id;
        }
        return $model;

    }

}
