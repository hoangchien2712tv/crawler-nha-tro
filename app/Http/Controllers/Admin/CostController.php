<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cost;

class CostController extends Controller
{
    public function index()
    {
        $data = Cost::all();
        return view('admin.cost.index',
            ['data' => $data,]);
    }
    public function create()
    {
        return view('admin.cost.create');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:cost',
            'slug' => 'required|string|max:255|unique:cost',
            'value' => 'required|integer',
            'status' => 'required'
        ]);
        $models = new Cost();
        $models->name = $request->name;
        $models->slug = $request->slug;
        $models->value = $request->value;
        $models->status = $request->status == 'on' ? 1 : 0;
        if ($models->save()) {
            return redirect()->to('/backend/cost/index')->with('status', 'Thêm mới giá thành công');
        }
    }
    public function edit($id)
    {
        $data = Cost::find($id);
        return view('admin.cost.edit', ['data'=>$data]);
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
            'value' => 'required|integer',
            'status' => 'required'
        ]);
        $model = Cost::find($request->id);
        $model->name = $request->name;
        $model->slug = $request->slug;
        $model->value = $request->value;
        $model->status = $request->status == 'on' ? 1 : 0;
        if ($model->save()) {
            return redirect()->to('/backend/cost/index')->with('status', 'Cập nhật thành công');
        }
    }
    public function delete($id){
        $data = Cost::find($id);
        $data->delete();
        return redirect()->to('/backend/cost/index')->with('status', 'Xóa thành công');
    }


}
