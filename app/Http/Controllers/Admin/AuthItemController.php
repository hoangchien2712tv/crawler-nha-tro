<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AuthItemRequest;
use App\Models\AuthItem;
use App\Models\AuthItemChild;


class AuthItemController extends Controller
{
    public function index()
    {
        $permissions = AuthItem::where('type', 2)->get();
        $groupPer = AuthItem::where('type', 1)->get();

        return view('admin.authitem.index',
            [
                'permissions' => $permissions,
                'groupPer' => $groupPer
            ]);
    }

    public function create()
    {
        return view('admin.authitem.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            'rule_name' => 'required'
        ]);
        $models = new AuthItem();

        $models->name = $request->name;
        $models->type = $request->type;
        $models->desc = $request->desc;
        $models->rule_name = $request->rule_name;
        if ($models->save()) {
            return redirect()->to('/backend/role/index')->with('status', 'thành công');
        }

    }

    public function edit($id)
    {
        $models = $this->model($id);
        return view('admin.authitem.update', ['models' => $models]);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            'rule_name' => 'required'
        ]);
        $models = $this->model($id);

        $models->name = $request->name;
        $models->type = $request->type;
        $models->desc = $request->desc;
        $models->rule_name = $request->rule_name;
        if ($models->save()) {
            return redirect()->to('/backend/role/index')->with('status', 'cập nhật thành công');
        }
    }


    public function delete($id)
    {
        $data = AuthItem::find($id);
        $data->delete();
        return redirect()->to('/backend/role/index')->with('status', 'xóa thành công');
    }

    public function view($id)
    {
        $models = $this->model($id);


        $assigned = AuthItemChild::where('parent', $models->rule_name)->get();
        $notIn = [];
        foreach ($assigned as $assign) {
            $notIn[] = $assign->child;
        }
        $permissions = AuthItem::where('type', 2)->whereNotIn('rule_name', $notIn)->get();

        return view('admin.authitem.viewrole',
            [
                'models' => $models,
                'permissions' => $permissions,
                'assigned' => $assigned,
            ]);
    }

    public function assignPerToRole(Request $request)
    {
        var_dump($_POST);
        $ruleName = $request->rule_name;

        if ($request->role_assign) {
            foreach ($request->role_assign as $value) {
                $models = new AuthItemChild();
                $models->parent = $ruleName;
                $models->child = $value;
                $models->save();
            }
        }

        if ($request->role_assigned) {

            foreach ($request->role_assigned as $value) {
                AuthItemChild::where('child', $value)->delete();
            }
        }
        return redirect()->to('/backend/role/view/' . $request->id);
    }

    public function model($id)
    {
        $model = AuthItem::find($id);
        if (!$model) {
            return 'error ' . $id;
        }
        return $model;

    }


}
