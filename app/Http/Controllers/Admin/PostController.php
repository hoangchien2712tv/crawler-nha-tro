<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
   public function index()
   {
      $posts = Post::orderBy('id', 'desc')->paginate(10);
      return view('admin.post.index');
   }

   public function create(Request $request)
    {
       return view('admin.post.create');
    }
}
