<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TypeOfNews;

class TypeOfNewsController extends Controller
{
    public function index()
    {
        $data = TypeOfNews::all();
        return view('admin.typeOfNews.index',
            [
                'data' => $data,
            ]);
    }

    public function create()
    {
        return view('admin.typeOfNews.create');
    }

    public function store(Request $request)
    {
        $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
            'day_bundle' => array('required', 'regex:' . $regex),
            'week_bundle' => array('required', 'regex:' . $regex),
            'month_bundle' => array('required', 'regex:' . $regex),
            'status' => 'required'
        ]);
        $models = new TypeOfNews();
        $models->name = $request->name;

        $models->slug = $request->slug;
        $models->day_bundle = $request->day_bundle;
        $models->week_bundle = $request->week_bundle;
        $models->month_bundle = $request->month_bundle;
        $models->status = $request->status == 'on' ? 1 : 0;
        try {
            if ($models->save()) {
                return redirect()->to('/backend/typeofnews/index')->with('status', 'Thêm mới kiêu thành công');
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

    }


    public function edit($id)
    {
        $data = TypeOfNews::find($id);
        return view('admin.typeOfNews.edit', ['data'=>$data]);
    }

    public function update(Request $request)
    {
        $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
            'slug' => 'required',
            'day_bundle' => array('required', 'regex:' . $regex),
            'week_bundle' => array('required', 'regex:' . $regex),
            'month_bundle' => array('required', 'regex:' . $regex),
            'status' => 'required'
        ]);
        $model = TypeOfNews::find($request->id);
        $model->name = $request->name;
        $model->slug = $request->slug;
        $model->day_bundle = $request->day_bundle;
        $model->week_bundle = $request->week_bundle;
        $model->month_bundle = $request->month_bundle;
        $model->status = $request->status == 'on' ? 1 : 0;
        try {
            if ($model->save()) {
                return redirect()->to('/backend/typeofnews/index')->with('status', 'Thêm mới kiêu thành công');
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function delete($id){
        $model = TypeOfNews::find($id);
        $model->delete();
        return redirect()->to('/backend/typeofnews/index')->with('status', 'Xóa thành công');
    }



}
