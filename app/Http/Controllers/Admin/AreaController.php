<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Area;

class AreaController extends Controller
{
    public function index()
    {
        $data = Area::all();
        return view('admin.area.index',
            ['data' => $data,]);
    }
    public function create()
    {
        return view('admin.area.create');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:area',
            'slug' => 'required|string|max:255|unique:area',
            'value' => 'required|integer',
            'status' => 'required'
        ]);
        $models = new Area();
        $models->name = $request->name;
        $models->slug = $request->slug;
        $models->value = $request->value;
        $models->status = $request->status == 'on' ? 1 : 0;
        if ($models->save()) {
            return redirect()->to('/backend/area/index')->with('status', 'Thêm mới giá thành công');
        }
    }
    public function edit($id)
    {
        $data = Area::find($id);
        return view('admin.area.edit', ['data'=>$data]);
    }
    public function update(Request $request)
    {
        $this->validate(request(), [
            'id' => 'required',
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
            'value' => 'required|integer',
            'status' => 'required'
        ]);
        $model = Area::find($request->id);
        $model->name = $request->name;
        $model->slug = $request->slug;
        $model->value = $request->value;
        $model->status = $request->status == 'on' ? 1 : 0;
        if ($model->save()) {
            return redirect()->to('/backend/area/index')->with('status', 'Cập nhật thành công');
        }
    }
    public function delete($id){
        $data = Area::find($id);
        $data->delete();
        return redirect()->to('/backend/area/index')->with('status', 'Xóa thành công');
    }

}
