<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Province;

class ProvinceController extends Controller
{
    public function index(){
        $data = Province::all();
        return view('admin.province.index', ['data'=>$data]);
    }

}
