<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


/**
 * WebCrawlerController
 * @author Crawler
 */
class TestController extends Controller
{


    public function index()
    {
        print "Starting\n";
        $url = 'https://static.thuephongtro.com/Uploads/Images/Large/20180926010812-20180925_105757.jpg';
        $image = imagecreatefromstring(file_get_contents($url));
        print "Image is loaded\n";
        if(!$image) return false;
        print "Success\n";

    }

    public function convertBase64ToImage($photo = null, $path = null)
    {
        if (!empty($photo)) {
            $photo = str_replace('data:image/png;base64,', '', $photo);
            $photo = str_replace(' ', '+', $photo);
            $photo = str_replace('data:image/jpeg;base64,', '', $photo);
            $photo = str_replace('data:image/gif;base64,', '', $photo);
            $entry = base64_decode($photo);
            $image = imagecreatefromstring($entry);

            $fileName = time() . ".jpeg";
            $directory = $this->pathUrl($fileName);

            header('Content-type:image/jpeg');

            if (!empty($path)) {
                if (file_exists($path)) {
                    unlink($path);
                }
            }

            $saveImage = imagejpeg($image, $directory);
            imagedestroy($image);
            if ($saveImage) {
                return $fileName;
            } else {
                return false; // image not saved
            }
        }
    }

    public function pathUrl($fileName)
    {
        $publicPath = public_path('/uploads/images/');
        $this->mkdir($publicPath);
//      $pathDir = $publicPath . date("Y/m/d");
        return '/uploads/images/' . date("Y/m/d") . '/' . $fileName . '.jpg';
    }

    public function mkdir($path)
    {
        if (!is_dir($path . date("Y/m/d"))) {
            mkdir($path . date("Y/m/d"), 0755, true);
        }
    }

}