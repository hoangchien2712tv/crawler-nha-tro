<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The roles that belong to the user.
     */
    public function roles()
    {
        return $this->hasMany('App\Models\AuthAssign', 'user_id', 'id');
//      return $this->belongsToMany('App\Role')->withPivot('user_id', 'role_id');
    }

    public function authItem()
    {
        return $this->hasManyThrough('App\Models\AuthItem', 'App\Models\AuthAssign', 'user_id', 'rule_name', 'id', 'auth_item_name');
    }

    public function authItemChild()
    {
        return $this->hasManyThrough('App\Models\AuthItemChild', 'App\Models\AuthAssign', 'user_id', 'parent', 'id', 'auth_item_name');
    }


    public function hasAllPerMission($role)
    {

//        $allPer = [];
        $permision = User::find(Auth::id())->authItem()->where('type', 2)->where('rule_name', $role)->get();
        if (!$permision->isEmpty()) {
            return true;
        }
        $perOfRole = User::find(Auth::id())->authItemChild()->where('child', $role)->get();
        if (!$perOfRole->isEmpty()) {
            return true;
        }
//        foreach ($permision as $per) {
//            $allPer[] = $per->rule_name;
//        }
//        foreach ($perOfRole as $perRole) {
//            $allPer[] = $perRole->child;
//        }
//        if (in_array($role, $allPer)) {
//            return true;
//        }
        return false;

    }

    /*role of user*/

    public function role()
    {
        return $this->hasMany('\App\Models\AuthAssign', 'id', 'user_id');
    }

    // SELECT * FROM `auth_assign` join auth_item on auth_item.rule_name = auth_assign.auth_item_name JOIN auth_item_child on auth_item_child.parent = auth_item.rule_name where auth_assign.user_id = 2


    public function hasAnyRole($roles)
    {
        var_dump($roles);
        foreach ($this->role() as $key => $roleUser) {
            if ($roles == $roleUser->auth_item_name) {
                break;
                return true;
            }
        }
        // Check if the user is a root account
        return false;
    }


}
