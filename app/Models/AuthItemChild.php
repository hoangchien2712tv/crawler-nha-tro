<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuthItemChild extends Model
{
    protected $table = 'auth_item_child';
    protected $fillable = [
        'parent', 'child',
    ];

    public function role(){
        return $this->hasOne('\App\Models\AuthItem','rule_name','child');
    }

    public function childs(){
        return $this->hasMany('\App\Models\AuthItemChild','parent','child');
    }



}
