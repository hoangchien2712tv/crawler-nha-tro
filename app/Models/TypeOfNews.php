<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeOfNews extends Model
{
   protected $table = 'type_of_news';
}
