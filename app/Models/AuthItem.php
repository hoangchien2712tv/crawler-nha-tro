<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Notifiable;

class AuthItem extends Model
{
    protected $table = 'auth_item';

    protected $fillable = [
        'name', 'type', 'description','rule_name'
    ];

    public function items(){
        return $this->hasMany('\App\Models\AuthItemChild','parent','rule_name');
    }


}
