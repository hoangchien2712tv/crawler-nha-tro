<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use DB;
class AuthAssign extends Model
{
    protected $table = 'auth_assign';

    protected $fillable = [
        'user_id', 'auth_item_name'
    ];
    public function hasRole(){
        return $this->hasOne('\App\Models\AuthItem','rule_name','auth_item_name');
    }


//    public function hasAllRole() {
//        $hasAllRole = DB::table('auth_assign')
//            ->join('auth_item', 'auth_item.rule_name ', '=', 'auth_assign.auth_item_name')
//            ->join('auth_item_child', 'auth_item_child.parent', '=', 'auth_item.rule_name')
//            ->where('auth_assign.user_id', Auth::user()->id)
//            ->get();
//            return $hasAllRole;
//    }
}
