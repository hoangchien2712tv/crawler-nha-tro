<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Notifiable;
class News extends Model
{
   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $table = 'news';
   protected $fillable = [
    //   'title', 'slug',
   ];
   public function typeOfNews() {
      return $this->belongsTo('App\Models\TypeOfNews', 'type_of_news', 'id');
   }
   public function district() {
    return $this->hasOne('App\Models\District', 'district_id', 'district_id');
   }
   public function category() {
    return $this->hasOne('App\Models\Category', 'id', 'cate_id');
   }
}
