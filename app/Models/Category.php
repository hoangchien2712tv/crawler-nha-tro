<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    protected $fillable = [
        'title', 'slug', 'parent', 'status'
    ];

    public $data;
    public function getCategoryParent($parent = 0, $level = '')
    {
        $result = Category::where('parent', $parent)->select('id', 'title')->get();
        $level .='--| ';
        foreach ($result as $value) {
            if($parent == 0) {
                $level = '';
            }
            $this->data[$value->id] = $level.$value->{'title'};
            $this->getCategoryParent($value->id, $level);
        }
        return $this->data;
    }
    public function parents() {
//        return $this->hasOne('App\Models\Admin\Category', 'parent');
        return $this->belongsTo('App\Models\Category', 'parent');
    }




}
