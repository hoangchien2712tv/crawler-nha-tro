<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
class UserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:insert {numberOfUser=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
   public function handle()
   {
       $numberOfUser = $this->argument('numberOfUser');
       if($this->confirm('Are you sure create user?')) {
           $faker = Faker\Factory::create();
           try {
               for ($i=0; $i < $numberOfUser; $i++) {
                   DB::table('users')->insert([
                       'name'         => $faker->name,
                       'email'        => $faker->unique()->email,
                       'password'     =>  Hash::make('123456')
                   ]);
               }
               $this->info($numberOfUser  . ' user create success');
           } catch (Exception $e) {
               $this->error('Error ' . $e . ' when create users.');
           }
       }
   }

}
