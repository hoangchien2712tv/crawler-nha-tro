<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use GuzzleHttp\Client;
use App\Models\News;
use App\Models\Images;
use App\Models\Cost;
use App\Models\Area;
use App\Models\Province;
use App\Models\District;

class BatDongSan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:batdongsan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client();
        $url = 'https://batdongsan.com.vn/cho-thue-nha-tro-phong-tro';
        $res = $client->request('GET', $url);
        $html = $res->getBody();
        $html = new \Htmldom($html);
        $list_item = $html->find('.product-list .search-productItem');
        foreach ($list_item as $key => $item) {

            $href = $item->find('.p-title h3 a', 0)->href;
            $proDis = $item->find('.product-city-dist', 0)->plaintext;
            $proDis = explode(',', $proDis);
            $province = trim($proDis[1]);
            $district = trim($proDis[0]);

            $detail = new Client();
            $resDetail = $detail->request('GET', $url . $href);
            $resDetail = $resDetail->getBody();
            $htmlDetail = new \Htmldom($resDetail);
            $pageSon = $htmlDetail->find('.prd-more-info', 0);
            /*nhiều trang củ chuối*/
            $check = $pageSon->find('div', 4)->plaintext;
            if ($this->check($check) == false) {
                continue; /*check xem ngày hết hạn có phù hợp không*/
            }

            $news = new News();

            $province = Province::where('name', 'like', '%' . $province . '%')->first();
            $news->province_id = $province->province_id;
            $district = District::where('name', 'like', '%' . $district . '%')->first();
            $news->district_id = $district->district_id;

            $news->title = $htmlDetail->find('.pm-title h1', 0)->plaintext;
            $news->slug = $this->makeAlias($htmlDetail->find('.pm-title h1', 0)->plaintext);
            $cost = (float)$htmlDetail->find('.gia-title strong', 0)->plaintext;

            $news->cost = $cost;
            $news->cost_id = $this->costRound($cost);
            $area = (float)$htmlDetail->find('.gia-title strong', 1)->plaintext;

            $news->area = $area;
            $news->area_id = $this->areaRound($area);

            $news->desc = htmlentities($htmlDetail->find('.pm-desc', 0)->plaintext, ENT_QUOTES, 'UTF-8');
            $cate_id = $htmlDetail->find('.div-table .table-detail .row .right', 0)->plaintext;

            $news->cate_id = $this->category(trim($cate_id));

            //Cho thuê nhà trọ, phòng trọ
            $news->address = $htmlDetail->find('.div-table .table-detail .row .right', 1)->plaintext;
            $user = $htmlDetail->find('#LeftMainContent__productDetail_contactName');
            if (!empty($user)) {
                $news->user = $htmlDetail->find('#LeftMainContent__productDetail_contactName .right', 0)->plaintext;
            }
//            $news->phone = (int)$htmlDetail->find('#LeftMainContent__productDetail_contactPhone .right', 0)->plaintext;
            $news->phone = (int)$htmlDetail->find('#LeftMainContent__productDetail_contactMobile .right', 0)->plaintext;

            $thumbs = $htmlDetail->find('#thumbs li');
            if (!empty($thumbs)) {
                $thumbImg = $thumbs[0]->find('img', 0)->src;
                if ($this->checkImgSize($thumbImg) == true) {
                    echo $news->thumbnail = $this->copyResizeImg($this->convertImage($thumbImg, 50),
                        160, 140);
                }
            }

            try {
                if ($news->save()) {
                    if (!empty($thumbs)) {
                        foreach ($thumbs as $thumb) {
                            $img = new Images();
                            $img->news_id = $news->id;
                            $imgSon = str_replace('200x200', '745x510', $thumb->find('img', 0)->src);
                            if ($this->checkImgSize($imgSon) == false) {
                                continue;
                            }
                            $img->image = $this->convertImage($imgSon, 50);
                            $img->save();
                        }
                    }
                }
            } catch (\Exception $e) {
                echo '<pre>';
                echo $e->getMessage();
                echo '</pre>';
            }
        }
    }

    function checkImgSize($url)
    {
        if (@getimagesize($url) && $this->is_url_exist($url) == true) {
            return true;
        } else {
            return false;
        }
    }

    public function category($cate_id)
    {
        switch ($cate_id) {
            case 'Cho thuê nhà trọ, phòng trọ':
                return 1;
                break;
            case 'Cho thuê nhà riêng':
                return 2;
                break;
            case 'Cho thuê căn hộ chung cư':
                return 3;
                break;
            case 'Tìm người ở ghép':
                return 4;
                break;
            case 'Cho thuê mặt bằng':
                return 5;
                break;
        }
    }

    public function areaRound($area)
    {
        if ($area < 20) {
            return 1;
        } elseif ($area < 30 && $area >= 20) {
            return 2;
        } elseif ($area < 50 && $area >= 30) {
            return 3;
        } elseif ($area < 60 && $area >= 50) {
            return 4;
        } elseif ($area < 70 && $area >= 60) {
            return 5;
        } elseif ($area < 80 && $area >= 70) {
            return 6;
        } elseif ($area < 90 && $area >= 80) {
            return 7;
        } elseif ($area < 100 && $area >= 90) {
            return 8;
        } elseif ($area >= 100) {
            return 9;
        }
    }


    public function costRound($cost)
    {
        if ($cost < 1) {
            return 1;
        } elseif ($cost < 2 && $cost >= 1) {
            return 2;
        } elseif ($cost < 3 && $cost >= 2) {
            return 3;
        } elseif ($cost < 5 && $cost >= 3) {
            return 4;
        } elseif ($cost < 7 && $cost >= 5) {
            return 5;
        } elseif ($cost < 10 && $cost >= 7) {
            return 6;
        } elseif ($cost < 15 && $cost >= 10) {
            return 7;
        } elseif ($cost >= 15) {
            return 8;
        }
    }

    public function check($check)
    {
        $check = str_replace('Ngày hết hạn:', '', $check);
        $check = str_replace('/', '-', $check);
        $check = date('Y-m-d', strtotime($check));
        $current = date('Y-m-d');
        if ($check < $current) {
            return false;
        }
        return true;
    }

    function makeAlias($str, $lowerCase = true)
    {
        $search = array(
            '#(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)#',
            '#(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)#',
            '#(ì|í|ị|ỉ|ĩ)#',
            '#(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)#',
            '#(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)#',
            '#(ỳ|ý|ỵ|ỷ|ỹ)#',
            '#(đ)#',
            '#(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)#',
            '#(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)#',
            '#(Ì|Í|Ị|Ỉ|Ĩ)#',
            '#(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)#',
            '#(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)#',
            '#(Ỳ|Ý|Ỵ|Ỷ|Ỹ)#',
            '#(Đ)#',
            "/[^a-zA-Z0-9\-\_]/",
        );
        $replace = array(
            'a',
            'e',
            'i',
            'o',
            'u',
            'y',
            'd',
            'A',
            'E',
            'I',
            'O',
            'U',
            'Y',
            'D',
            '-',
        );
        $str = preg_replace($search, $replace, $str);
        $str = preg_replace('/(-)+/', '-', $str);
        $str = preg_replace('/^-+|-+$/', '', $str);
        if ($lowerCase)
            $str = strtolower($str);
        return $str;
    }

    function convertImage($originalImage, $quality)
    {
        $ext = $this->ext($originalImage);

        $pathImg = $this->pathUrl($originalImage);
        if (preg_match('/jpg|jpeg/i', $ext)) {
            $imageTmp = imagecreatefromjpeg($originalImage);
        } else if (preg_match('/png/i', $ext)) {
            $imageTmp = imagecreatefrompng($originalImage);
        } else if (preg_match('/gif/i', $ext)) {
            $imageTmp = imagecreatefromgif($originalImage);
        } else if (preg_match('/bmp/i', $ext)) {
            $imageTmp = imagecreatefromwbmp($originalImage);
        } else {
            return false;
        }
//        $imageTmp = imagecreatefromstring($originalImage);

// quality is a value from 0 (worst) to 100 (best)
        imagejpeg($imageTmp, public_path($pathImg), $quality);
        imagedestroy($imageTmp);
        return $pathImg;
    }

    public function copyResizeImg($pathImg, $width_resize, $height_resize)
    {

        $pathImgThumb = $this->pathUrl($pathImg, true);
        list($width, $height) = getimagesize(public_path($pathImg));
// Load
        $thumb = imagecreatetruecolor($width_resize, $height_resize);
        $source = imagecreatefromjpeg(public_path($pathImg));
// Resize
        imagecopyresized($thumb, $source, 0, 0, 0, 0, $width_resize, $height_resize, $width, $height);
// Output
        imagejpeg($thumb, public_path($pathImgThumb));
        return $pathImgThumb;
    }

    public function ext($originalImage)
    {
        $exploded = explode('.', basename($originalImage));
        return $exploded[1];
    }

    public function pathUrl($originalImage, $thumb = false)
    {
        $exploded = explode('.', basename($originalImage));
        $nameImg = $exploded[0];
        $publicPath = public_path('/uploads/images/');
        $this->mkdir($publicPath);
//      $pathDir = $publicPath . date("Y/m/d");
        return '/uploads/images/' . date("Y/m/d") . '/' . ($thumb ? 'thumb-' : '') . $nameImg . '.jpg';
    }

    public function mkdir($path)
    {
        if (!is_dir($path . date("Y/m/d"))) {
            mkdir($path . date("Y/m/d"), 0755, true);
        }
    }

    function url_valid(&$url)
    {
        $file_headers = @get_headers($url);
        if ($file_headers === false) return false; // when server not found
        foreach ($file_headers as $header) { // parse all headers:
            // corrects $url when 301/302 redirect(s) lead(s) to 200:
            if (preg_match("/^Location: (http.+)$/", $header, $m)) $url = $m[1];
            // grabs the last $header $code, in case of redirect(s):
            if (preg_match("/^HTTP.+\s(\d\d\d)\s/", $header, $m)) $code = $m[1];
        } // End foreach...
        if ($code == 200) return true; // $code 200 == all OK
        else return false; // All else has failed, so this must be a bad link
    } // End function url_exists

    function is_url_exist($url)
    {
        $ch = @curl_init($url);
        @curl_setopt($ch, CURLOPT_NOBODY, true);
        @curl_exec($ch);
        $code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($code == 200) {
            $status = true;
        } else {
            $status = false;
        }
        curl_close($ch);
        return $status;
    }


}
