<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use GuzzleHttp\Client;
use App\Models\News;
use App\Models\Images;
use App\Models\Cost;
use App\Models\Area;
use App\Models\Province;
use App\Models\District;

class PhongTro123 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:phongtro123';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $html = new \Htmldom('https://phongtro123.com/tim-kiem?type=1&tinh=0&quan=0&duong=0&price=0&dientich=0');
        $list_item = $html->find('.block-left div.wrapper ul.list-post', 0)->find('li.post-item');
        foreach ($list_item as $key => $item) {
            $news = new News();
            $href = $item->find('a.post-link', 0)->href;
            $detail = new \Htmldom($href);

            $proDis = $item->find('span.address strong a', 0)->plaintext;
            $proDis = explode(',', $proDis);
            if ($detail->find('#da_het_han', 0) == true) {
                continue;
            }

            $province = Province::where('name', 'like', '%' . trim($proDis[1]) . '%')->first();
            $news->province_id = $province->province_id;
            $district = District::where('name', 'like', '%' . trim($proDis[0]) . '%')->first();
            $news->district_id = $district->district_id;

            $news->title = $detail->find('h1.post-title-lg a', 0)->plaintext;
            $news->slug = $this->makeAlias($detail->find('h1.post-title-lg a', 0)->plaintext);

            $son_detail = $detail->find('.post_summary .summary_row');
            $news->address = $son_detail[0]->find('.summary_item_info', 0)->plaintext;

            $cate_id = $son_detail[1]->find('.post_summary_left h2 a', 0)->plaintext;
            $news->cate_id = $this->category($cate_id);

            $news->user = $son_detail[2]->find('.post_summary_left .summary_item_info', 0)->plaintext;
            $news->updated_at = date("Y-m-d H:i:s");
            /*lấy ra ngày hết hạn*/
            $datetime = explode(" ", $son_detail[4]->find('.post_summary_left .summary_item_info', 0)->plaintext);
            $datetime = trim($datetime[1]) . ' ' . trim($datetime[0]);
            $date = strtotime(str_replace('/', '-', $datetime));
            $news->expiration_date = date('Y-m-d H:i:s', $date);
            /*lấy ra ngày hết hạn*/
            /*right*/
            $renter = $son_detail[1]->find('.post_summary_right .summary_item_info', 0)->plaintext;
            if ($renter == 'Tất cả') {
                $renter = 1;
            } else if ($renter == 'Nam') {
                $renter = 2;
            } else {
                $renter = 3;
            }
            $news->renter = $renter;
            $news->phone = (int)$son_detail[2]->find('.post_summary_right .summary_item_info_phone a span', 0)->plaintext;
            $area = (float)str_replace('m²', '', $son_detail[3]->find('.post_summary_right .summary_item_info_area', 0)->plaintext);

            $news->area = $area;
            $news->area_id = $this->areaRound($area);

            $costold = $son_detail[4]->find('.post_summary_right .summary_item_info_price', 0)->plaintext;

            if (strpos($costold, 'Nghìn/tháng') !== false) {
                $cost = 0.001 * ((float)str_replace('Nghìn/tháng', '', $costold));
            } else {
                $cost = (float)str_replace('Triệu/tháng', '', $costold);
            }

            $news->cost = $cost;
            $news->cost_id = $this->costRound($cost);

            $news->desc = htmlentities($detail->find('#motachitiet p', 0), ENT_QUOTES, 'UTF-8');

            $images = $detail->find('ul.slides li');

            if (!empty($images) && $this->checkImgSize($images[0]->find('img', 0)->src) == true) {
                $news->thumbnail = $this->copyResizeImg($this->convertImage($images[0]->find('img', 0)->src, 50),
                    160, 140);

                /*end lấy chi tiết thông tin*/
                try {
                    if ($news->save()) {

                        if (!empty($images)) {
                            foreach ($images as $image) {
                                $img = new Images();
                                $img->news_id = $news->id;
                                $imgSon = $image->find('img', 0)->src;
                                if ($this->checkImgSize($imgSon) == false) {
                                    continue;
                                }
                                $img->image = $this->convertImage($imgSon, 50);
                                $img->save();
                            }
                        }
                    }
                } catch (\Exception $e) {
                    echo '<pre>';
                    echo $e->getMessage();
                    echo '</pre>';
                }
            }
        }
    }

    function checkImgSize($url)
    {
        if (@getimagesize($url) && $this->ext($url) == true) {
            return true;
        } else {
            return false;
        }
    }

    public function category($cate_id)
    {
        switch ($cate_id) {
            case 'Cho thuê phòng trọ':
                return 1;
                break;
            case 'Nhà cho thuê':
                return 2;
                break;
            case 'Cho thuê căn hộ':
                return 3;
                break;
            case 'Tìm người ở ghép':
                return 4;
                break;
            case 'Cho thuê mặt bằng':
                return 5;
                break;
        }
    }

    public function areaRound($area)
    {
        if ($area < 20) {
            return 1;
        } elseif ($area < 30 && $area >= 20) {
            return 2;
        } elseif ($area < 50 && $area >= 30) {
            return 3;
        } elseif ($area < 60 && $area >= 50) {
            return 4;
        } elseif ($area < 70 && $area >= 60) {
            return 5;
        } elseif ($area < 80 && $area >= 70) {
            return 6;
        } elseif ($area < 90 && $area >= 80) {
            return 7;
        } elseif ($area < 100 && $area >= 90) {
            return 8;
        } elseif ($area >= 100) {
            return 9;
        }
    }


    public function costRound($cost)
    {
        if ($cost < 1) {
            return 1;
        } elseif ($cost < 2 && $cost >= 1) {
            return 2;
        } elseif ($cost < 3 && $cost >= 2) {
            return 3;
        } elseif ($cost < 5 && $cost >= 3) {
            return 4;
        } elseif ($cost < 7 && $cost >= 5) {
            return 5;
        } elseif ($cost < 10 && $cost >= 7) {
            return 6;
        } elseif ($cost < 15 && $cost >= 10) {
            return 7;
        } elseif ($cost >= 15) {
            return 8;
        }
    }

    public function check($check)
    {
        $check = str_replace('Ngày hết hạn:', '', $check);
        $check = str_replace('/', '-', $check);
        $check = date('Y-m-d H:i:s', strtotime($check));
        $current = date('Y-m-d H:i:s');
        if ($check < $current) {
            return false;
        }
        return true;
    }

    function makeAlias($str, $lowerCase = true)
    {
        $search = array(
            '#(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)#',
            '#(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)#',
            '#(ì|í|ị|ỉ|ĩ)#',
            '#(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)#',
            '#(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)#',
            '#(ỳ|ý|ỵ|ỷ|ỹ)#',
            '#(đ)#',
            '#(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)#',
            '#(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)#',
            '#(Ì|Í|Ị|Ỉ|Ĩ)#',
            '#(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)#',
            '#(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)#',
            '#(Ỳ|Ý|Ỵ|Ỷ|Ỹ)#',
            '#(Đ)#',
            "/[^a-zA-Z0-9\-\_]/",
        );
        $replace = array(
            'a',
            'e',
            'i',
            'o',
            'u',
            'y',
            'd',
            'A',
            'E',
            'I',
            'O',
            'U',
            'Y',
            'D',
            '-',
        );
        $str = preg_replace($search, $replace, $str);
        $str = preg_replace('/(-)+/', '-', $str);
        $str = preg_replace('/^-+|-+$/', '', $str);
        if ($lowerCase)
            $str = strtolower($str);
        return $str;
    }

    function convertImage($originalImage, $quality)
    {
        $ext = $this->ext($originalImage);

        $pathImg = $this->pathUrl($originalImage);
        if (preg_match('/jpg|jpeg/i', $ext)) {
            $imageTmp = @imagecreatefromjpeg($originalImage);
        } else if (preg_match('/png/i', $ext)) {
            $imageTmp = @imagecreatefrompng($originalImage);
        } else if (preg_match('/gif/i', $ext)) {
            $imageTmp = @imagecreatefromgif($originalImage);
        } else if (preg_match('/bmp/i', $ext)) {
            $imageTmp = @imagecreatefromwbmp($originalImage);
        } else {
            return false;
        }

// quality is a value from 0 (worst) to 100 (best)
        Header("Content-Type: image/jpeg");
        @imagejpeg($imageTmp, public_path($pathImg), $quality);
        @imagedestroy($imageTmp);
        return $pathImg;
    }

    public function copyResizeImg($pathImg, $width_resize, $height_resize)
    {

        $pathImgThumb = $this->pathUrl($pathImg, true);
        list($width, $height) = @getimagesize(public_path($pathImg));
// Load
        $thumb = @imagecreatetruecolor($width_resize, $height_resize);
        $source = @imagecreatefromjpeg(public_path($pathImg));
// Resize
        @imagecopyresized($thumb, $source, 0, 0, 0, 0, $width_resize, $height_resize, $width, $height);
// Output
        @imagejpeg($thumb, public_path($pathImgThumb));
        return $pathImgThumb;
    }

    public function ext($originalImage)
    {
        $exploded = explode('.', basename($originalImage));
        if (count($exploded) < 3) {
            return $exploded[1];
        }
        return false;
    }

    public function pathUrl($originalImage, $thumb = false)
    {
        $exploded = explode('.', basename($originalImage));
        if (count($exploded) < 3) {
            $nameImg = $exploded[0];
            $publicPath = public_path('/uploads/images/');
            $this->mkdir($publicPath);
//      $pathDir = $publicPath . date("Y/m/d");
            return '/uploads/images/' . date("Y/m/d") . '/' . ($thumb ? 'thumb-' : '') . $nameImg . '.jpg';
        }
        return false;

    }

    public function mkdir($path)
    {
        if (!is_dir($path . date("Y/m/d"))) {
            mkdir($path . date("Y/m/d"), 0755, true);
        }
    }

    function url_valid(&$url)
    {
        $file_headers = @get_headers($url);
        if ($file_headers === false) return false; // when server not found
        foreach ($file_headers as $header) { // parse all headers:
            // corrects $url when 301/302 redirect(s) lead(s) to 200:
            if (preg_match("/^Location: (http.+)$/", $header, $m)) $url = $m[1];
            // grabs the last $header $code, in case of redirect(s):
            if (preg_match("/^HTTP.+\s(\d\d\d)\s/", $header, $m)) $code = $m[1];
        } // End foreach...
        if ($code == 200) return true; // $code 200 == all OK
        else return false; // All else has failed, so this must be a bad link
    } // End function url_exists

}
