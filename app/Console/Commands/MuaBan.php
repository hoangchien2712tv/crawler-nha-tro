<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use GuzzleHttp\Client;
use App\Models\News;
use App\Models\Images;
use App\Models\Cost;
use App\Models\Area;
use App\Models\Province;
use App\Models\District;

class MuaBan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:muaban';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = 'https://muaban.net/nha-tro-phong-tro-toan-quoc-l0-c3405?img=1&time=3&cp=1';
        $html = new \Htmldom($url);
        $list_item = $html->find('.mbn-box-list', 3)->find('.mbn-box-list-content');
        foreach ($list_item as $key => $item) {

            $proDis = $item->find('.mbn-address', 0)->plaintext;
            $proDis = explode(',', $proDis);

            $province = trim(str_replace('TP.HCM', 'Hồ Chí Minh', $proDis[1]));
            $district = trim($proDis[0]);

            $href = $item->find('a', 1)->href;

            $news = new News();
            $news->title = $item->find('a', 1)->find('h2', 0)->plaintext;
            $news->slug = $this->makeAlias($item->find('a', 1)->find('h2', 0)->plaintext);
//
            $province = Province::where('name', 'like', '%' . $province . '%')->first();
            $news->province_id = $province->province_id;
            $district = District::where('name', 'like', '%' . $district . '%')->first();
            $news->district_id = $district->district_id;


            $detail = new \Htmldom($href);
            $news->address = $detail->find('.ct-tech ul li', 0)->find('div div', 1)->plaintext;
            $area = (float)$detail->find('.ct-tech ul li', 7)->find('div div', 1)->plaintext;
            $news->area_id = $this->areaRound($area);
            $news->area = $area;

            if(!$detail->find('.price', 0)){
                continue;
            }
            $cost = ((float)str_replace('.', '', $detail->find('.price', 0)->plaintext)) / 1000000;

            $news->cost = $cost;
            $news->cost_id = $this->costRound($cost);

            $news->cate_id = 1;

            $news->user = $detail->find('.ct-contact', 0)->find('div', 1)->plaintext;

            if (!empty($detail->find('.ct-contact', 0)->find('div', 7))) {
                $news->phone = (int)$detail->find('.ct-contact', 0)->find('div', 7)->find('b', 0)->plaintext;

            } else if (!empty($detail->find('.ct-contact', 0)->find('div', 4))) {
                $news->phone = (int)$detail->find('.ct-contact', 0)->find('div', 4)->find('b', 0)->plaintext;
            } else {
                continue;
            }
            $news->desc = htmlentities($detail->find('.ct-body', 0)->plaintext, ENT_QUOTES, 'UTF-8');

            $images = $detail->find('.ct-image .item');

            if (!empty($images)) {
                $thumbImg = $images[0]->find('img', 0)->attr['data-src'];
                if ($this->checkImgSize($thumbImg) == false) {
                    continue;
                }
                $news->thumbnail = $this->copyResizeImg($this->convertImage($thumbImg, 50),
                    160, 140);
            } else {
                continue;
            }

            try {
                if ($news->save()) {
                    if (!empty($images)) {
                        foreach ($images as $image) {
                            $img = new Images();
                            $img->news_id = $news->id;
                            $imgSon = $image->find('img', 0)->attr['data-src'];
                            if ($this->checkImgSize($imgSon) == false) {
                                continue;
                            }
                            $img->image = $this->convertImage($imgSon, 50);
                            $img->save();
                        }
                    }
                }

            } catch (\Exception $e) {
                echo '<pre>';
                echo $e->getMessage();
                echo '</pre>';

            }
            echo $key;
        }
    }

    function checkImgSize($url)
    {
        if (@getimagesize($url) && $this->ext($url) == true) {
            return true;
        } else {
            return false;
        }
    }

    public function category($cate_id)
    {
        switch ($cate_id) {
            case 'Cho thuê phòng trọ':
                return 1;
                break;
            case 'Nhà cho thuê':
                return 2;
                break;
            case 'Cho thuê căn hộ':
                return 3;
                break;
            case 'Tìm người ở ghép':
                return 4;
                break;
            case 'Cho thuê mặt bằng':
                return 5;
                break;
        }
    }

    public function areaRound($area)
    {
        if ($area < 20) {
            return 1;
        } elseif ($area < 30 && $area >= 20) {
            return 2;
        } elseif ($area < 50 && $area >= 30) {
            return 3;
        } elseif ($area < 60 && $area >= 50) {
            return 4;
        } elseif ($area < 70 && $area >= 60) {
            return 5;
        } elseif ($area < 80 && $area >= 70) {
            return 6;
        } elseif ($area < 90 && $area >= 80) {
            return 7;
        } elseif ($area < 100 && $area >= 90) {
            return 8;
        } elseif ($area >= 100) {
            return 9;
        }
    }


    public function costRound($cost)
    {
        if ($cost < 1) {
            return 1;
        } elseif ($cost < 2 && $cost >= 1) {
            return 2;
        } elseif ($cost < 3 && $cost >= 2) {
            return 3;
        } elseif ($cost < 5 && $cost >= 3) {
            return 4;
        } elseif ($cost < 7 && $cost >= 5) {
            return 5;
        } elseif ($cost < 10 && $cost >= 7) {
            return 6;
        } elseif ($cost < 15 && $cost >= 10) {
            return 7;
        } elseif ($cost >= 15) {
            return 8;
        }
    }

    function makeAlias($str, $lowerCase = true)
    {
        $search = array(
            '#(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)#',
            '#(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)#',
            '#(ì|í|ị|ỉ|ĩ)#',
            '#(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)#',
            '#(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)#',
            '#(ỳ|ý|ỵ|ỷ|ỹ)#',
            '#(đ)#',
            '#(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)#',
            '#(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)#',
            '#(Ì|Í|Ị|Ỉ|Ĩ)#',
            '#(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)#',
            '#(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)#',
            '#(Ỳ|Ý|Ỵ|Ỷ|Ỹ)#',
            '#(Đ)#',
            "/[^a-zA-Z0-9\-\_]/",
        );
        $replace = array(
            'a',
            'e',
            'i',
            'o',
            'u',
            'y',
            'd',
            'A',
            'E',
            'I',
            'O',
            'U',
            'Y',
            'D',
            '-',
        );
        $str = preg_replace($search, $replace, $str);
        $str = preg_replace('/(-)+/', '-', $str);
        $str = preg_replace('/^-+|-+$/', '', $str);
        if ($lowerCase)
            $str = strtolower($str);
        return $str;
    }

    function convertImage($originalImage, $quality)
    {
        $ext = $this->ext($originalImage);

        $pathImg = $this->pathUrl($originalImage);
        if (preg_match('/jpg|jpeg/i', $ext)) {
            $imageTmp = imagecreatefromjpeg($originalImage);
        } else if (preg_match('/png/i', $ext)) {
            $imageTmp = imagecreatefrompng($originalImage);
        } else if (preg_match('/gif/i', $ext)) {
            $imageTmp = imagecreatefromgif($originalImage);
        } else if (preg_match('/bmp/i', $ext)) {
            $imageTmp = imagecreatefromwbmp($originalImage);
        } else {
            return false;
        }
// quality is a value from 0 (worst) to 100 (best)
        imagejpeg($imageTmp, public_path($pathImg), $quality);
        imagedestroy($imageTmp);
        return $pathImg;
    }

    public function copyResizeImg($pathImg, $width_resize, $height_resize)
    {

        $pathImgThumb = $this->pathUrl($pathImg, true);
        list($width, $height) = getimagesize(public_path($pathImg));
// Load
        $thumb = imagecreatetruecolor($width_resize, $height_resize);
        $source = imagecreatefromjpeg(public_path($pathImg));
// Resize
        imagecopyresized($thumb, $source, 0, 0, 0, 0, $width_resize, $height_resize, $width, $height);
// Output
        imagejpeg($thumb, public_path($pathImgThumb));
        return $pathImgThumb;
    }

    public function ext($originalImage)
    {
        $exploded = explode('.', basename($originalImage));
        return $exploded[1];
    }

    public function pathUrl($originalImage, $thumb = false)
    {
        $exploded = explode('.', basename($originalImage));
        $nameImg = $exploded[0];
        $publicPath = public_path('/uploads/images/');
        $this->mkdir($publicPath);
//      $pathDir = $publicPath . date("Y/m/d");
        return '/uploads/images/' . date("Y/m/d") . '/' . ($thumb ? 'thumb-' : '') . $nameImg . '.jpg';
    }

    public function mkdir($path)
    {
        if (!is_dir($path . date("Y/m/d"))) {
            mkdir($path . date("Y/m/d"), 0755, true);
        }
    }

    function url_valid(&$url)
    {
        $file_headers = @get_headers($url);
        if ($file_headers === false) return false; // when server not found
        foreach ($file_headers as $header) { // parse all headers:
            // corrects $url when 301/302 redirect(s) lead(s) to 200:
            if (preg_match("/^Location: (http.+)$/", $header, $m)) $url = $m[1];
            // grabs the last $header $code, in case of redirect(s):
            if (preg_match("/^HTTP.+\s(\d\d\d)\s/", $header, $m)) $code = $m[1];
        } // End foreach...
        if ($code == 200) return true; // $code 200 == all OK
        else return false; // All else has failed, so this must be a bad link
    } // End function url_exists


}
