<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use GuzzleHttp\Client;
use App\Models\News;
use App\Models\Images;
use App\Models\Cost;
use App\Models\Area;
use App\Models\Province;
use App\Models\District;

class ThuePhongTro extends Command
{
    /**
     * The name and signagit stture of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:thuephongtro';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = 'https://thuephongtro.com/tim-kiem.html?category=0400&province=&district=&area=&price=&id=';
        $client = new Client();
        $res = $client->request('GET', $url);
        $html = $res->getBody();
        $html = new \Htmldom($html);

        $list_item = $html->find('.content-left .for-user', 0)->find('ul li');
        $count = count($list_item);

        for ($i = 0; $i < $count - 1; $i++) {
            $href = $list_item[$i]->find('.desc .title-fix1 a', 0)->href;
            $detail = new Client();
            $resDetail = $detail->request('GET', 'https://thuephongtro.com' . $href);
            $resDetail = $resDetail->getBody();

            $htmlDetail = new \Htmldom($resDetail);
            $proDis = $htmlDetail->find('.content-left', 0)->find('.breadcrumb', 0)->find('li');

            if ($htmlDetail->find('.note-error', 0) == true) {
                continue;
            }

            $news = new News();

            $province = $proDis[2]->find('a span', 0)->plaintext;

            $province = str_replace('TP', '', trim($province));
            $district = trim($proDis[3]->find('a span', 0)->plaintext);

            $province = Province::where('name', 'like', '%' . $province . '%')->first();
            $news->province_id = $province->province_id;

            $district = District::where('name', 'like', '%' . $district . '%')->first();
            $news->district_id = $district->district_id;

            $news->title = $htmlDetail->find('h1', 0)->plaintext;
            $news->slug = $this->makeAlias($htmlDetail->find('h1', 0)->plaintext);

            $news->cate_id = $this->category(trim($proDis[1]->find('a span', 0)->plaintext));

            $detail = $htmlDetail->find('.row-desc .row');
            $cost = $detail[0]->find('.col2 span', 0)->plaintext;
            $cost = str_replace(',', '', $cost);
            $cost = (float)$cost * 0.000001;

            $news->cost = $cost;
            $news->cost_id = $this->costRound($cost);

            $area = (float)$detail[1]->find('.col2 span', 0)->plaintext;
            $news->area = $area;
            $news->area_id = $this->areaRound($area);
            $address = $detail[2]->find('.col2 span', 0)->plaintext;
            if (strlen($address) > 255) {
                continue;
            }
            $news->address = $detail[2]->find('.col2 span', 0)->plaintext;

            if (!$detail[3]->find('.col2 span', 0)) {
                continue;
            }
            $user = $detail[3]->find('.col2 span', 0)->plaintext;

            $news->user = $user;

            $news->phone = (int)$detail[4]->find('.col2 span', 0)->plaintext;
            $news->desc = htmlentities($htmlDetail->find('.pd-desc .pd-desc-content', 0)->plaintext, ENT_QUOTES, 'UTF-8');

            $images = $htmlDetail->find('#myGallery li');


            if (!$images) {
                continue;
//                file_put_contents(public_path($this->pathUrl($thumbImg, true)), $thumbImg);
//                $news->thumbnail = $this->pathUrl($thumbImg, true);
            }
            $thumbImg = $images[0]->find('a', 0)->href;
            if($this->checkImgSize($thumbImg) == true) {
                $news->thumbnail = $this->copyResizeImg($this->convertImage($thumbImg, 50),
                    160, 140);
            }


            try {
                if ($news->save()) {
                    if (!empty($images)) {
                        foreach ($images as $image) {
                            $img = new Images();
                            $img->news_id = $news->id;
                            $imgSon = $image->find('a', 0)->href;
                            if ($this->checkImgSize($imgSon) == false) {
                                continue;
                            }
                            $img->image = $this->convertImage($imgSon, 50);
                            $img->save();
                        }
                    }
                }
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
            echo $i . '<br>';
        }
    }

    function checkImgSize($url)
    {
        if (@getimagesize($url) && $this->is_url_exist($url)) {
            return true;
        } else {
            return false;
        }
    }

    public function category($cate_id)
    {
        switch ($cate_id) {
            case 'Cho thuê phòng trọ':
                return 1;
                break;
            case 'Nhà cho thuê':
                return 2;
                break;
            case 'Cho thuê căn hộ':
                return 3;
                break;
            case 'Tìm người ở ghép':
                return 4;
                break;
            case 'Cho thuê mặt bằng':
                return 5;
                break;
        }
    }

    public function areaRound($area)
    {
        if ($area < 20) {
            return 1;
        } elseif ($area < 30 && $area >= 20) {
            return 2;
        } elseif ($area < 50 && $area >= 30) {
            return 3;
        } elseif ($area < 60 && $area >= 50) {
            return 4;
        } elseif ($area < 70 && $area >= 60) {
            return 5;
        } elseif ($area < 80 && $area >= 70) {
            return 6;
        } elseif ($area < 90 && $area >= 80) {
            return 7;
        } elseif ($area < 100 && $area >= 90) {
            return 8;
        } elseif ($area >= 100) {
            return 9;
        }
    }

    public function costRound($cost)
    {
        if ($cost < 1) {
            return 1;
        } elseif ($cost < 2 && $cost >= 1) {
            return 2;
        } elseif ($cost < 3 && $cost >= 2) {
            return 3;
        } elseif ($cost < 5 && $cost >= 3) {
            return 4;
        } elseif ($cost < 7 && $cost >= 5) {
            return 5;
        } elseif ($cost < 10 && $cost >= 7) {
            return 6;
        } elseif ($cost < 15 && $cost >= 10) {
            return 7;
        } elseif ($cost >= 15) {
            return 8;
        }
    }

    function makeAlias($str, $lowerCase = true)
    {
        $search = array(
            '#(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)#',
            '#(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)#',
            '#(ì|í|ị|ỉ|ĩ)#',
            '#(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)#',
            '#(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)#',
            '#(ỳ|ý|ỵ|ỷ|ỹ)#',
            '#(đ)#',
            '#(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)#',
            '#(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)#',
            '#(Ì|Í|Ị|Ỉ|Ĩ)#',
            '#(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)#',
            '#(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)#',
            '#(Ỳ|Ý|Ỵ|Ỷ|Ỹ)#',
            '#(Đ)#',
            "/[^a-zA-Z0-9\-\_]/",
        );
        $replace = array(
            'a',
            'e',
            'i',
            'o',
            'u',
            'y',
            'd',
            'A',
            'E',
            'I',
            'O',
            'U',
            'Y',
            'D',
            '-',
        );
        $str = preg_replace($search, $replace, $str);
        $str = preg_replace('/(-)+/', '-', $str);
        $str = preg_replace('/^-+|-+$/', '', $str);
        if ($lowerCase)
            $str = strtolower($str);
        return $str;
    }

    function convertImage($originalImage, $quality)
    {
        $ext = $this->ext($originalImage);

        $pathImg = $this->pathUrl($originalImage);

        if (preg_match('/jpg|jpeg/i', $ext)) {
            $imageTmp = @imagecreatefromstring(file_get_contents($originalImage));
        } else if (preg_match('/png/i', $ext)) {
            $imageTmp = @imagecreatefrompng($originalImage);
        } else if (preg_match('/gif/i', $ext)) {
            $imageTmp = @imagecreatefromgif($originalImage);
        } else if (preg_match('/bmp/i', $ext)) {
            $imageTmp = @imagecreatefromwbmp($originalImage);
        } else {
            return false;
        }
// quality is a value from 0 (worst) to 100 (best)
        @imagejpeg($imageTmp, public_path($pathImg), $quality);
        @imagedestroy($imageTmp);
        return $pathImg;
    }

    public function copyResizeImg($pathImg, $width_resize, $height_resize)
    {

        $pathImgThumb = $this->pathUrl($pathImg, true);
        list($width, $height) = @getimagesize(public_path($pathImg));
// Load
        $thumb = @imagecreatetruecolor($width_resize, $height_resize);
        $source = @imagecreatefromjpeg(public_path($pathImg));
// Resize
        @imagecopyresized($thumb, $source, 0, 0, 0, 0, $width_resize, $height_resize, $width, $height);
// Output
        @imagejpeg($thumb, public_path($pathImgThumb));
        return $pathImgThumb;
    }


    public function ext($originalImage)
    {
        $exploded = explode('.', basename($originalImage));
        if (count($exploded) < 3) {
            return $exploded[1];
        }
        return false;
    }

    public function pathUrl($originalImage, $thumb = false)
    {
        $exploded = explode('.', basename($originalImage));
        $nameImg = $exploded[0];
        $publicPath = public_path('/uploads/images/');
        $this->mkdir($publicPath);
//      $pathDir = $publicPath . date("Y/m/d");
        return '/uploads/images/' . date("Y/m/d") . '/' . ($thumb ? 'thumb-' : '') . $nameImg . '.jpg';
    }

    public function mkdir($path)
    {
        if (!is_dir($path . date("Y/m/d"))) {
            mkdir($path . date("Y/m/d"), 0755, true);
        }
    }

    function url_valid(&$url)
    {
        $file_headers = @get_headers($url);
        if ($file_headers === false) return false; // when server not found
        foreach ($file_headers as $header) { // parse all headers:
            // corrects $url when 301/302 redirect(s) lead(s) to 200:
            if (preg_match("/^Location: (http.+)$/", $header, $m)) $url = $m[1];
            // grabs the last $header $code, in case of redirect(s):
            if (preg_match("/^HTTP.+\s(\d\d\d)\s/", $header, $m)) $code = $m[1];
        } // End foreach...
        if ($code == 200) return true; // $code 200 == all OK
        else return false; // All else has failed, so this must be a bad link
    } // End function url_exists

    function is_url_exist($url)
    {
        $ch = @curl_init($url);
        @curl_setopt($ch, CURLOPT_NOBODY, true);
        @curl_exec($ch);
        $code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($code == 200) {
            $status = true;
        } else {
            $status = false;
        }
        curl_close($ch);
        return $status;
    }


}
