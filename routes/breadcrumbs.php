<?php

// Home
Breadcrumbs::register('Trang chủ', function ($breadcrumbs) {
    $breadcrumbs->push('Trang chủ', route('backend'));
});

// Home > About
Breadcrumbs::register('Tin tức', function ($breadcrumbs) {
    $breadcrumbs->parent('Trang chủ');
    $breadcrumbs->push('Tin tức', route('backend.news.index'));
});
// Home > Blog > [Category] > [Post]
Breadcrumbs::register('Thêm mới', function ($breadcrumbs) {
    $breadcrumbs->parent('Tin tức');
    $breadcrumbs->push('Thêm mới', route('backend.news.create'));
});
// Home > Blog
Breadcrumbs::register('blog', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Blog', route('blog'));
});

// Home > Blog > [Category]
Breadcrumbs::register('category', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('blog');
    $breadcrumbs->push($category->title, route('category', $category->id));
});

