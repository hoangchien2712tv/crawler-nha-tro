<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');

Route::get('/index', 'PostController@index');
Route::post('/posts', 'PostController@store');

Route::get('image/index', 'ImageController@index');
Route::post('image/upload', 'ImageController@upload');
Route::get('email', 'EmailController@sendEmail');

Route::get('/clone', 'CloneController@index');
Route::get('/crawler', 'WebCrawlerController@index');
Route::get('/test', 'TestController@index');
Route::get('/lay-tin', 'LayTinController@index');
Route::get('/chotot', 'ChototController@index');
Route::get('/muaban', 'MuaBanController@index');
Route::get('/thuephongtro', 'ThuePhongTroController@index');
Route::get('/duclich', 'DuLichController@index');


Auth::routes();

Route::get('/backend', 'HomeController@index')->name('backend');
Route::get('/logout', 'Auth\LoginController@logout');
/*
 * Admin
 */
Route::group(['middleware' => ['auth']], function () {

    Route::get('/backend/user/profile', [
        'uses' => '\App\Http\Controllers\Admin\UserController@profile',
    ]);
    Route::post('/change-password', [
        'uses' => '\App\Http\Controllers\Admin\UserController@changePassword',
    ]);
    Route::post('/update-profile', [
        'uses' => '\App\Http\Controllers\Admin\UserController@updateProfile',
    ]);


    Route::get('/getDistrict', '\App\Http\Controllers\Admin\NewsController@getDistrict');
    Route::get('/getStreet', '\App\Http\Controllers\Admin\NewsController@getStreet');
    /*loại tin*/
    Route::get('/backend/typeofnews/index', '\App\Http\Controllers\Admin\TypeOfNewsController@index');

    Route::get('/backend/typeofnews/create', '\App\Http\Controllers\Admin\TypeOfNewsController@create');
    Route::post('/backend/typeofnews/store', '\App\Http\Controllers\Admin\TypeOfNewsController@store');

    Route::get('/backend/typeofnews/edit/{id}', '\App\Http\Controllers\Admin\TypeOfNewsController@edit');
    Route::post('/backend/typeofnews/update', '\App\Http\Controllers\Admin\TypeOfNewsController@update');

    Route::get('/backend/typeofnews/delete/{id}', '\App\Http\Controllers\Admin\TypeOfNewsController@delete');
    /*giá*/
    Route::get('/backend/cost/index', '\App\Http\Controllers\Admin\CostController@index');
    Route::get('/backend/cost/create', '\App\Http\Controllers\Admin\CostController@create');
    Route::post('/backend/cost/store', '\App\Http\Controllers\Admin\CostController@store');
    Route::get('/backend/cost/edit/{id}', '\App\Http\Controllers\Admin\CostController@edit');
    Route::post('/backend/cost/update', '\App\Http\Controllers\Admin\CostController@update');
    Route::get('/backend/cost/delete/{id}', '\App\Http\Controllers\Admin\CostController@delete');

    /*area*/

    Route::get('backend/area/index',
        [
            'uses' => '\App\Http\Controllers\Admin\AreaController@index',
            'roles' => 'area-index'
        ]);
    Route::get('backend/area/create',
        [
            'uses' => '\App\Http\Controllers\Admin\AreaController@create',
            'roles' => 'area-index'
        ]);
    Route::post('backend/area/store',
        [
            'uses' => '\App\Http\Controllers\Admin\AreaController@store',
            'roles' => 'area-index'
        ]);
    Route::get('backend/area/edit/{id}',
        [
            'uses' => '\App\Http\Controllers\Admin\AreaController@edit',
            'roles' => 'area-index'
        ]);
    Route::get('backend/area/delete/{id}',
        [
            'uses' => '\App\Http\Controllers\Admin\AreaController@delete',
            'roles' => 'area-index'
        ]);
    Route::post('backend/area/update',
        [
            'uses' => '\App\Http\Controllers\Admin\AreaController@update',
            'roles' => 'area-index'
        ]);

// role and permission





//    Route::group(['middleware' => ['roles']], function () {
    /*tin*/
    Route::get('/backend/news/create',
        [
            'uses' => '\App\Http\Controllers\Admin\NewsController@create',
            'roles' => 'news-create',
            'as' => 'backend.news.create'
        ]);

    Route::post('/backend/news/store',
        [
            'uses' => '\App\Http\Controllers\Admin\NewsController@store',
            'as' => 'backend.news.store',
            'roles' => 'news-create',
        ]);

    Route::get('/backend/news/view/{id}',
        [
            'uses' => '\App\Http\Controllers\Admin\NewsController@view',
            'roles' => 'news-view'
        ]);

    Route::get('/backend/news/edit/{id}',
        [
            'uses' => '\App\Http\Controllers\Admin\NewsController@edit',
            'as' => 'backend.news.edit',
            'roles' => 'news-edit'
        ]
    );
    Route::get('/backend/news/delete/{id}',
        [
            'uses' => '\App\Http\Controllers\Admin\NewsController@delete',
            'roles' => 'news-delete'
        ]
    );
    Route::post('/backend/news/update',
        [
            'as' => 'backend.news.update',
            'uses' => '\App\Http\Controllers\Admin\NewsController@update',
            'roles' => 'news-edit',
        ]);

    Route::get('/backend/news/index', [
        'uses' => '\App\Http\Controllers\Admin\NewsController@index',
        'roles' => 'news-index',
        'as' => 'backend.news.index',
    ]);

    Route::get('/backend/news/liveSearch', [
        'uses' => '\App\Http\Controllers\Admin\NewsController@liveSearch',
        'as' => 'backend.news.liveSearch',
        'roles' => 'news-index'
    ]);

    Route::get('/backend/file/index', [
        'uses' => '\App\Http\Controllers\Admin\FileController@index',
        'roles' => 'file-index'
    ]);

    Route::get('/backend/category/index', [
        'uses' => '\App\Http\Controllers\Admin\CategoryController@index',
        'roles' => 'category-index'
    ]);
    Route::get('/backend/category/create', [
        'uses' => '\App\Http\Controllers\Admin\CategoryController@create',
        'roles' => 'category-create'
    ]);
    Route::get('/backend/category/store', [
        'uses' => '\App\Http\Controllers\Admin\CategoryController@store',
        'roles' => 'category-store'
    ]);
    Route::get('/backend/category/edit/{id}', [
        'uses' => '\App\Http\Controllers\Admin\CategoryController@edit',
        'roles' => 'category-edit'
    ]);
    Route::post('/backend/category/update', [
        'uses' => '\App\Http\Controllers\Admin\CategoryController@update',
        'roles' => 'category-update'
    ]);


    Route::get('backend/user/index',
        [
            'uses' => '\App\Http\Controllers\Admin\UserController@index',
            'as' => 'backend.user.index',
            'roles' => 'user-index'
        ]);
    Route::get('backend/user/create',
        [
            'uses' => '\App\Http\Controllers\Admin\UserController@create',
            'roles' => 'user-create'
        ]);
    Route::get('/backend/user/edit/{id}', [
        'uses' => '\App\Http\Controllers\Admin\UserController@edit',
        'roles' => 'user-update'
    ]);
    Route::post('backend/user/update/',
        [
            'uses' => '\App\Http\Controllers\Admin\UserController@update',
            'roles' => 'user-update',
            'as' => 'backend.user.update'
        ]);

    Route::get('backend/user/delete/{id}',
        [
            'uses'=>'\App\Http\Controllers\Admin\UserController@delete',
            'roles' => 'user-delete'
        ]);

    Route::get('backend/user/view/{id}',
        [
            'uses' => '\App\Http\Controllers\Admin\UserController@view',
            'as' => 'backend.user.view',
            'roles' => 'user-view'
        ]);
    Route::post('backend/user/assignPerToRole',
        [
            'uses' => '\App\Http\Controllers\Admin\UserController@assignPerToRole',
            'as' => 'backend.user.assignpertorole',
            'roles' => 'user-assignPerToRole'
        ]);
    Route::post('backend/user/create', [
        'uses'=>'\App\Http\Controllers\Admin\UserController@store',
        'as'=>'backend.user.create',
        'roles' => 'authItem'
    ]);

    Route::post('backend/role/store',
            [
                'uses' => '\App\Http\Controllers\Admin\AuthItemController@store',
                'as' => 'backend.role.create',
                'roles' => 'authItem'
            ]);
    Route::get('backend/role/delete/{id}',
            [
                'uses' => '\App\Http\Controllers\Admin\AuthItemController@delete',
                'roles' => 'authItem'
            ]);

    Route::post('backend/role/assignpertorole',
        [
            'uses' => '\App\Http\Controllers\Admin\AuthItemController@assignPerToRole',
            'as' => 'backend.role.assignpertorole',
            'roles' => 'authItem'
        ]);
    Route::get('backend/role/view/{id}',
        [
            'uses' => '\App\Http\Controllers\Admin\AuthItemController@view',
            'as' => 'backend.role.view',
            'roles' => 'authItem'
        ]);
    Route::post('backend/role/update/{id}',
        [
            'uses' => '\App\Http\Controllers\Admin\AuthItemController@update',
            'as' => 'backend.role.update',
            'roles' => 'authItem'
        ]);
        Route::get('backend/role/edit/{id}',
            [
                'uses' => '\App\Http\Controllers\Admin\AuthItemController@edit',
                'roles' => 'authItem'
            ]);
    Route::get('backend/role/index',
        [
            'uses' => '\App\Http\Controllers\Admin\AuthItemController@index',
            'as' => 'backend.role',
            'roles' => 'authItem'
        ]);
    Route::get('backend/role/create',
        [
            // 'middleware' => ['roles'],
            'uses' => '\App\Http\Controllers\Admin\AuthItemController@create',
//            'as' => 'backend.role.create',
            'roles' => 'authItem'
        ]);
    /*province*/
    Route::get('backend/province/index',
        [
            'uses' => '\App\Http\Controllers\Admin\ProvinceController@index',
            'roles' => 'province-index'
        ]);



//    });
});



