<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('address');

            $table->string('user', 64)->nullable()->default(null);;

            $table->tinyInteger('renter')->default(1);
            $table->integer('phone')->nullable()->default(null);
            $table->float('area');
            $table->float('cost');
            $table->text('desc');
            $table->string('thumbnail')->nullable()->default(null);
            $table->tinyInteger('cate_id');
            $table->tinyInteger('area_id');
            $table->tinyInteger('cost_id');

            $table->integer('province_id');
            $table->integer('district_id');
            $table->integer('street_id')->nullable()->default(null);

            $table->integer('type_of_news')->default(5);
            $table->integer('date_bundle_id')->default(1);
            $table->integer('date_number_id')->nullable()->default(null);


            $table->dateTime('expiration_date')->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
