<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $this->call([
            ProvinceTableSeeder::class,
            DistrictTableSeeder::class,
        ]);

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
        ]);
        for ($i = 0; $i < 10; $i++) {
            DB::table('users')->insert([
                'name' => str_random(10),
                'email' => str_random(10) . '@gmail.com',
                'password' => bcrypt('123456'),
            ]);
        }
        DB::table('auth_item')->insert(
            [
                'name' => 'Nhóm User',
                'type' => 1,
                'rule_name' => 'user',
            ]);
        DB::table('auth_item')->insert(
            [
                'name' => 'Quyền xem list user',
                'type' => 2,
                'rule_name' => 'user-index',
            ]);
        DB::table('auth_item')->insert(
            [
                'name' => 'Quyền sửa User',
                'type' => 2,
                'rule_name' => 'user-edit',
            ]);
        DB::table('auth_item')->insert(
            [
                'name' => 'Quyền xem list News',
                'type' => 2,
                'rule_name' => 'news-index',
            ]);
        DB::table('auth_item')->insert(
            [
                'name' => 'Quyền xem sửa News',
                'type' => 2,
                'rule_name' => 'news-edit',
            ]);
        DB::table('auth_item')->insert(
            [
                'name' => 'Nhóm News',
                'type' => 1,
                'rule_name' => 'news',
            ]);
        DB::table('auth_item_child')->insert(
            [
                'parent' => 'user',
                'child' => 'user-index',
            ]);
        DB::table('auth_item_child')->insert(
            [
                'parent' => 'user',
                'child' => 'user-edit',
            ]);
        DB::table('auth_item_child')->insert(
            [
                'parent' => 'news',
                'child' => 'news-index',
            ]);
        DB::table('auth_item_child')->insert(
            [
                'parent' => 'news',
                'child' => 'news-edit',
            ]);


        DB::table('auth_assign')->insert(
            [
                'user_id' => 1,
                'auth_item_name' => 'user',
            ]);
        DB::table('auth_assign')->insert(
            [
                'user_id' => 1,
                'auth_item_name' => 'user-index',
            ]);
        DB::table('auth_assign')->insert(
            [
                'user_id' => 1,
                'auth_item_name' => 'news',
            ]);


        DB::table('category')->insert(
            [
                'id' => 1,
                'title' => 'Cho thuê phòng trọ',
                'slug' => 'cho-thue-phong-tro',
                'parent' => 0,
                'status' => 1,
            ]);
        DB::table('category')->insert(
            [
                'id' => 2,
                'title' => 'Nhà cho thuê',
                'slug' => 'nha-cho-thue',
                'parent' => 0,
                'status' => 1,
            ]);
        DB::table('category')->insert(
            [
                'id' => 3,
                'title' => 'Cho thuê căn hộ',
                'slug' => 'cho-thue-can-ho',
                'parent' => 0,
                'status' => 1,
            ]);
        DB::table('category')->insert(
            [
                'id' => 4,
                'title' => 'Tìm người ở ghép',
                'slug' => 'tim-nguoi-o-ghep',
                'parent' => 0,
                'status' => 1,
            ]);
        DB::table('category')->insert(
            [
                'id' => 5,
                'title' => 'Cho thuê mặt bằng',
                'slug' => 'cho-thue-mat-bang',
                'parent' => 0,
                'status' => 1,
            ]);


        DB::table('cost')->insert(
            [
                'name' => 'Dưới 1 triệu',
                'slug' => '1',
                'value' => 1,
                'status' => 1,
            ]);
        DB::table('cost')->insert(
            [
                'name' => '1 triệu - 2 triệu',
                'slug' => '1-2',
                'value' => 2,
                'status' => 1,
            ]);
        DB::table('cost')->insert(
            [
                'name' => '2 triệu - 3 triệu',
                'slug' => '2-3',
                'value' => 3,
                'status' => 1,
            ]);
        DB::table('cost')->insert(
            [
                'name' => '3 triệu - 5 triệu',
                'slug' => '3-5',
                'value' => 4,
                'status' => 1,
            ]);
        DB::table('cost')->insert(
            [
                'name' => '5 triệu - 7 triệu',
                'slug' => '5-7',
                'value' => 5,
                'status' => 1,
            ]);
        DB::table('cost')->insert(
            [
                'name' => '7 triệu - 10 triệu',
                'slug' => '7-10',
                'value' => 6,
                'status' => 1,
            ]);
        DB::table('cost')->insert(
            [
                'name' => '10 triệu - 15 triệu',
                'slug' => '10-15',
                'value' => 7,
                'status' => 1,
            ]);
        DB::table('cost')->insert(
            [
                'name' => 'Trên 15 triệu',
                'slug' => '15',
                'value' => 8,
                'status' => 1,
            ]);

        DB::table('area')->insert(
            [
                'name' => 'Dưới 20 m2',
                'slug' => '20',
                'value' => 1,
                'status' => 1,
            ]);
        DB::table('area')->insert(
            [
                'name' => '20 m2 30 m2',
                'slug' => '20-30',
                'value' => 2,
                'status' => 1,
            ]);
        DB::table('area')->insert(
            [
                'name' => '30 m2 50 m2',
                'slug' => '30-50',
                'value' => 3,
                'status' => 1,
            ]);
        DB::table('area')->insert(
            [
                'name' => '50 m2 60 m2',
                'slug' => '50-60',
                'value' => 4,
                'status' => 1,
            ]);
        DB::table('area')->insert(
            [
                'name' => '60 m2 70 m2',
                'slug' => '60-70',
                'value' => 5,
                'status' => 1,
            ]);
        DB::table('area')->insert(
            [
                'name' => '70 m2 80 m2',
                'slug' => '70-80',
                'value' => 6,
                'status' => 1,
            ]);
        DB::table('area')->insert(
            [
                'name' => '80 m2 90 m2',
                'slug' => '80-90',
                'value' => 7,
                'status' => 1,
            ]);
        DB::table('area')->insert(
            [
                'name' => '90 m2 100 m2',
                'slug' => '90-100',
                'value' => 8,
                'status' => 1,
            ]);
        DB::table('area')->insert(
            [
                'name' => 'Trên 100 m2',
                'slug' => '100',
                'value' => 9,
                'status' => 1,
            ]);
//
        DB::table('type_of_news')->insert(
            [
                'name' => 'Tin VIP nổi bật',
                'slug' => 'tin-vip-noi-bat',
                'day_bundle' => 50000,
                'week_bundle' => 315000,
                'month_bundle' => 1200000,
                'status' => 1,
            ]);
        DB::table('type_of_news')->insert(
            [
                'name' => 'Tin VIP 1',
                'slug' => 'tin-vip-1',
                'day_bundle' => 30000,
                'week_bundle' => 190000,
                'month_bundle' => 800000,
                'status' => 1,
            ]);
        DB::table('type_of_news')->insert(
            [
                'name' => 'Tin VIP 2',
                'slug' => 'tin-vip-2',
                'day_bundle' => 20000,
                'week_bundle' => 133000,
                'month_bundle' => 540000,
                'status' => 1,
            ]);
        DB::table('type_of_news')->insert(
            [
                'name' => 'Tin VIP 3',
                'slug' => 'tin-vip-3',
                'day_bundle' => 10000,
                'week_bundle' => 63000,
                'month_bundle' => 240000,
                'status' => 1,
            ]);
        DB::table('type_of_news')->insert(
            [
                'name' => 'Tin thường',
                'slug' => 'tin-thường',
                'day_bundle' => 2000,
                'week_bundle' => 12000,
                'month_bundle' => 48000,
                'status' => 1,
            ]);
        
        DB::table('time_bundle')->insert(
            [
                'name' => 'Đăng theo ngày',
            ]);
        DB::table('time_bundle')->insert(
            [
                'name' => 'Đăng theo tuần',
            ]);
        DB::table('time_bundle')->insert(
            [
                'name' => 'Đăng theo tháng',
            ]);

    }
}

